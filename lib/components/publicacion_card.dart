import 'package:app_aforo/models/Calificacion.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';

class PublicacionCard extends StatelessWidget {
  final Calificacion calificacion;

  const PublicacionCard({required this.calificacion});

  @override
  Widget build(BuildContext context) {
    final fechaHoraRegistro = DateTime.fromMillisecondsSinceEpoch(
        calificacion.fechaHoraRegistro!.toSeconds() * 1000);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Material(
        elevation: 5,
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: Colors.white,
            border: Border.all(color: kGreenPrimaryColor),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Hero(
                  tag: '${calificacion.id}',
                  child: Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                          '${calificacion.urlImagenUsuario}',
                        ),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(
                          60,
                        ),
                      ),
                      border: Border.all(
                        color: kGreenPrimaryColor,
                        width: 2,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${calificacion.nombresUsuario}',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                        ),
                        textAlign: TextAlign.start,
                      ),
                      if (calificacion.comentario != null)
                        SizedBox(
                          height: 10,
                        ),
                      if (calificacion.comentario != null)
                        Text(
                          '${calificacion.comentario ?? ''}',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                          ),
                          textAlign: TextAlign.start,
                        ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          RatingBar.builder(
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            itemSize: 20,
                            onRatingUpdate: (rating) {},
                            initialRating: calificacion.puntaje!,
                            ignoreGestures: true,
                          ),
                          Expanded(
                            child: Text(
                              '${DateFormat('dd/MM/yyyy - hh:mm a').format(fechaHoraRegistro)}',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                              ),
                              textAlign: TextAlign.end,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
