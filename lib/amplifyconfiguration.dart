const amplifyconfig = ''' {
    "UserAgent": "aws-amplify-cli/2.0",
    "Version": "1.0",
    "api": {
        "plugins": {
            "awsAPIPlugin": {
                "appaforo": {
                    "endpointType": "GraphQL",
                    "endpoint": "https://yat7lpgxtjfizlkgubte24g5ha.appsync-api.us-east-2.amazonaws.com/graphql",
                    "region": "us-east-2",
                    "authorizationType": "API_KEY",
                    "apiKey": "da2-rp4cp42mvraj3eeg4bwp2eyr6m"
                }
            }
        }
    },
    "auth": {
        "plugins": {
            "awsCognitoAuthPlugin": {
                "UserAgent": "aws-amplify-cli/0.1.0",
                "Version": "0.1.0",
                "IdentityManager": {
                    "Default": {}
                },
                "AppSync": {
                    "Default": {
                        "ApiUrl": "https://yat7lpgxtjfizlkgubte24g5ha.appsync-api.us-east-2.amazonaws.com/graphql",
                        "Region": "us-east-2",
                        "AuthMode": "API_KEY",
                        "ApiKey": "da2-rp4cp42mvraj3eeg4bwp2eyr6m",
                        "ClientDatabasePrefix": "appaforo_API_KEY"
                    },
                    "appaforo_AWS_IAM": {
                        "ApiUrl": "https://yat7lpgxtjfizlkgubte24g5ha.appsync-api.us-east-2.amazonaws.com/graphql",
                        "Region": "us-east-2",
                        "AuthMode": "AWS_IAM",
                        "ClientDatabasePrefix": "appaforo_AWS_IAM"
                    },
                    "appaforo_AMAZON_COGNITO_USER_POOLS": {
                        "ApiUrl": "https://yat7lpgxtjfizlkgubte24g5ha.appsync-api.us-east-2.amazonaws.com/graphql",
                        "Region": "us-east-2",
                        "AuthMode": "AMAZON_COGNITO_USER_POOLS",
                        "ClientDatabasePrefix": "appaforo_AMAZON_COGNITO_USER_POOLS"
                    }
                },
                "CredentialsProvider": {
                    "CognitoIdentity": {
                        "Default": {
                            "PoolId": "us-east-2:f9fa8060-e406-48b8-ad53-4ebce3a3da28",
                            "Region": "us-east-2"
                        }
                    }
                },
                "CognitoUserPool": {
                    "Default": {
                        "PoolId": "us-east-2_GgPS1A2Xh",
                        "AppClientId": "3tp22gteaknenb4jg0pf516qut",
                        "Region": "us-east-2"
                    }
                },
                "Auth": {
                    "Default": {
                        "authenticationFlowType": "USER_SRP_AUTH",
                        "loginMechanisms": [
                            "EMAIL"
                        ],
                        "signupAttributes": [
                            "EMAIL"
                        ],
                        "passwordProtectionSettings": {
                            "passwordPolicyMinLength": 8,
                            "passwordPolicyCharacters": []
                        },
                        "mfaConfiguration": "OFF",
                        "mfaTypes": [
                            "SMS"
                        ]
                    }
                },
                "S3TransferUtility": {
                    "Default": {
                        "Bucket": "appaforo686885949d074267856a95ed96f837d2215654-dev",
                        "Region": "us-east-2"
                    }
                }
            }
        }
    },
    "storage": {
        "plugins": {
            "awsS3StoragePlugin": {
                "bucket": "appaforo686885949d074267856a95ed96f837d2215654-dev",
                "region": "us-east-2",
                "defaultAccessLevel": "guest"
            }
        }
    }
}''';