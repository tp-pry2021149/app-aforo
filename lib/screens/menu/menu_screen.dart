import 'package:app_aforo/screens/menu/favorito_screen.dart';
import 'package:app_aforo/screens/menu/gestionar_establecimiento_screen.dart';
import 'package:app_aforo/screens/menu/mapa_screen.dart';
import 'package:app_aforo/screens/menu/perfil_screen.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:flutter/material.dart';

class MenuScreen extends StatefulWidget {
  static const String id = 'menu_screen';
  final int? indexPage;
  const MenuScreen({this.indexPage});

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  late int _currentPageIndex =
      (widget.indexPage != null) ? widget.indexPage! : 1;
  final _pages = [
    MapaScreen(),
    FavoritoScreen(),
    GestionarEstablecimientoScreen(),
    PerfilScreen(),
  ];
  Widget _getCurrentPage() => _pages[_currentPageIndex];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _getCurrentPage(),
      bottomNavigationBar: Container(
        color: kGreenPrimaryColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            CustomIcon(
              currentPageIndex: _currentPageIndex,
              iconoSeleccionado: Icons.map,
              iconoNoSeleccionado: Icons.map_outlined,
              numPage: 0,
              onTap: (animationController) {
                setState(() {
                  animationController.forward(from: 0.0);
                  _currentPageIndex = 0;
                });
              },
            ),
            CustomIcon(
              currentPageIndex: _currentPageIndex,
              iconoSeleccionado: Icons.favorite,
              iconoNoSeleccionado: Icons.favorite_outline,
              numPage: 1,
              onTap: (animationController) {
                setState(() {
                  animationController.forward(from: 0.0);
                  _currentPageIndex = 1;
                });
              },
            ),
            CustomIcon(
              currentPageIndex: _currentPageIndex,
              iconoSeleccionado: Icons.add_business,
              iconoNoSeleccionado: Icons.add_business_outlined,
              numPage: 2,
              onTap: (animationController) {
                setState(() {
                  animationController.forward(from: 0.0);
                  _currentPageIndex = 2;
                });
              },
            ),
            CustomIcon(
              currentPageIndex: _currentPageIndex,
              iconoSeleccionado: Icons.person,
              iconoNoSeleccionado: Icons.person_outline,
              numPage: 3,
              onTap: (animationController) {
                setState(() {
                  animationController.forward(from: 0.0);
                  _currentPageIndex = 3;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}

class CustomIcon extends StatefulWidget {
  final int currentPageIndex;
  final IconData iconoSeleccionado;
  final IconData iconoNoSeleccionado;
  final Function onTap;
  final int numPage;

  const CustomIcon({
    required this.currentPageIndex,
    required this.iconoSeleccionado,
    required this.iconoNoSeleccionado,
    required this.onTap,
    required this.numPage,
  });

  @override
  _CustomIconState createState() => _CustomIconState();
}

class _CustomIconState extends State<CustomIcon>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(
        vsync: this,
        duration: const Duration(
          milliseconds: 600,
        ),
        value: 1.0);
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(6),
      child: AnimatedBuilder(
          animation: _animationController,
          builder: (context, _) {
            return Transform(
              transform: Matrix4.identity()
                ..setEntry(3, 2, 0.001)
                ..scale(_animationController.value),
              alignment: Alignment.center,
              child: InkWell(
                  child: Icon(
                    (widget.currentPageIndex == widget.numPage)
                        ? widget.iconoSeleccionado
                        : widget.iconoNoSeleccionado,
                    size: 32,
                    color: Colors.white,
                  ),
                  onTap: () {
                    widget.onTap(_animationController);
                  }),
            );
          }),
    );
  }
}
