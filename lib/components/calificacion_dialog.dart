import 'package:amplify_datastore/amplify_datastore.dart';
import 'package:app_aforo/components/rounded_button.dart';
import 'package:app_aforo/components/rounded_text_field.dart';
import 'package:app_aforo/models/Calificacion.dart';
import 'package:app_aforo/services/calificacion_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';

class CalificacionDialog extends StatefulWidget {
  final String establecimientoId;

  const CalificacionDialog({required this.establecimientoId});
  @override
  _CalificacionDialogState createState() => _CalificacionDialogState();
}

class _CalificacionDialogState extends State<CalificacionDialog> {
  final calificacionService = CalificacionService.instance;
  bool estaCargando = false;
  bool esFormularioValido = false;
  late String message;
  Calificacion calificacion = Calificacion();

  @override
  void initState() {
    super.initState();
    final usuario = Provider.of<UsuarioState>(context, listen: false).usuario;
    calificacion = calificacion.copyWith(
      apellidosUsuario: usuario.apellidos,
      nombresUsuario: usuario.nombres,
      urlImagenUsuario: usuario.urlImagen,
      usuarioId: usuario.id,
      establecimientoId: widget.establecimientoId,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: RatingBar.builder(
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      setState(() {
                        esFormularioValido = true;
                        calificacion = calificacion.copyWith(puntaje: rating);
                      });
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: 10,
                  ),
                  child: RoundedTextField(
                    texto: 'Comentario',
                    inputType: TextInputType.text,
                    obscureText: false,
                    onChange: (value) {
                      calificacion = calificacion.copyWith(comentario: value);
                    },
                    borderColor: kGreenPrimaryColor,
                    borderRadius: 30.0,
                    onTap: () {},
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 30,
                    vertical: 20,
                  ),
                  child: RoundedButton(
                    texto: 'Guardar',
                    color: esFormularioValido
                        ? kGreenPrimaryColor
                        : kLightGrayPrimaryColor,
                    onPressed: !esFormularioValido
                        ? null
                        : () async {
                            FocusScope.of(context).unfocus();
                            setState(() => estaCargando = true);
                            calificacion = calificacion.copyWith(
                                fechaHoraRegistro:
                                    TemporalTimestamp(DateTime.now()));
                            try {
                              await calificacionService.create(calificacion);
                              message = 'Gracias por su valoración';
                            } catch (e) {
                              message = (e is CustomException)
                                  ? e.mensaje
                                  : kMensajeErrorGenerico;
                            } finally {
                              setState(() => estaCargando = false);
                              Navigator.pop(context, message);
                            }
                          },
                  ),
                ),
              ],
            ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
