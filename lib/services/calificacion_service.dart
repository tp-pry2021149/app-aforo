import 'package:amplify_flutter/amplify.dart';
import 'package:app_aforo/models/Calificacion.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'dart:developer' as dev;

class CalificacionService {
  static CalificacionService _instance =
      CalificacionService._privateConstructor();
  static CalificacionService get instance => _instance;
  CalificacionService._privateConstructor();

  Future<void> create(Calificacion calificacion) async {
    dev.log('Creando calificación en DynamoDb $calificacion',
        name: 'CalificacionService.create');
    try {
      await Amplify.DataStore.save(calificacion);
    } catch (e) {
      dev.log(e.toString(), name: 'CalificacionService.create');
      throw CustomException("Ocurrió un error al crear calificación");
    }
  }

  Future<List<Calificacion>> getCalificaciones(
      {String? establecimientoId}) async {
    dev.log(
        'Obteniendo calificaciones del establecimiento con id: $establecimientoId',
        name: 'CalificacionService.getCalificaciones');
    try {
      List<Calificacion> calificaciones = await Amplify.DataStore.query(
        Calificacion.classType,
        where: Calificacion.ESTABLECIMIENTOID.eq(establecimientoId),
      );
      return calificaciones;
    } catch (e) {
      dev.log(e.toString(), name: 'CalificacionService.getCalificaciones');
      throw CustomException(
          "Ocurrió un error al obtener calificaciones del establecimiento");
    }
  }
}
