import 'package:app_aforo/models/Aforo.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AforoItem extends StatelessWidget {
  final Aforo aforo;
  final Function eliminar;

  AforoItem({required this.aforo, required this.eliminar});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
          border: Border.all(color: kGreenPrimaryColor),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  children: [
                    Icon(
                      Icons.group_rounded,
                      size: 25,
                      color: kGreenPrimaryColor,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Nombre: ${aforo.nombreEstablecimiento}',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Aforo: ${aforo.cantidad}/${aforo.aforoMaximoEstablecimiento}',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Porcentaje: ${((aforo.cantidad! / aforo.aforoMaximoEstablecimiento!) * 100).toStringAsFixed(2)}%',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Hora: ${DateFormat('hh:mm a').format(DateTime.fromMillisecondsSinceEpoch(aforo.fechaHoraRegistro!.toSeconds() * 1000))}',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  eliminar();
                },
                child: Icon(
                  Icons.delete,
                  size: 25,
                  color: Colors.red,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
