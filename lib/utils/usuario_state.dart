import 'dart:convert';
import 'package:app_aforo/models/ModelProvider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'constants.dart';

class UsuarioState extends ChangeNotifier {
  final _storage = new FlutterSecureStorage();
  late Usuario _usuario;

  Usuario get usuario => _usuario;

  Future<void> setUsuario(Usuario usuario) async {
    _usuario = usuario;
    await _storage.write(key: kStorage, value: jsonEncode(usuario.toJson()));
    notifyListeners();
  }

  Future<bool> existUsuario() async {
    var storageValue = await _storage.read(key: kStorage);
    if (storageValue != null) {
      _usuario = Usuario.fromJson(jsonDecode(storageValue));
      return true;
    }
    return false;
  }

  Future<void> deleteUsuario() async {
    _usuario = new Usuario();
    await _storage.deleteAll();
    notifyListeners();
  }
}
