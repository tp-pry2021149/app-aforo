import 'package:app_aforo/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RoundedTextField extends StatelessWidget {
  final String texto;
  final bool obscureText;
  final TextInputType inputType;
  final Function? onChange;
  final Color borderColor;
  final double borderRadius;
  final IconData? suffixIcon;
  final Function? onTap;
  final TextEditingController? controller;
  final bool? readOnly;
  final bool esNumeroEntero;

  RoundedTextField({
    required this.texto,
    this.onChange,
    required this.obscureText,
    required this.inputType,
    required this.borderColor,
    required this.borderRadius,
    this.suffixIcon,
    this.onTap,
    this.controller,
    this.readOnly = false,
    this.esNumeroEntero = false,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.all(
        Radius.circular(borderRadius),
      ),
      color: Colors.white,
      child: Container(
        height: 35,
        child: TextField(
          readOnly: readOnly!,
          textAlign: TextAlign.center,
          keyboardType: inputType,
          obscureText: obscureText,
          cursorColor: Colors.grey,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 14,
          ),
          decoration: InputDecoration(
            suffixIcon: (suffixIcon != null)
                ? Icon(
                    suffixIcon,
                    size: 30,
                  )
                : null,
            labelStyle: TextStyle(
              color: kGreenPrimaryColor,
              fontSize: 14.0,
            ),
            contentPadding: EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 30.0,
            ),
            labelText: texto,
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: borderColor,
                width: 1,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: borderColor,
                width: 1,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            isDense: true,
          ),
          onChanged: (value) => onChange!(value),
          controller: controller,
          onTap: () => onTap!(),
          inputFormatters: esNumeroEntero
              ? [
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                ]
              : [],
        ),
      ),
    );
  }
}
