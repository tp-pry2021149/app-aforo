import 'package:app_aforo/components/rounded_button.dart';
import 'package:app_aforo/components/rounded_drop_down_button.dart';
import 'package:app_aforo/components/rounded_text_field_with_validations.dart';
import 'package:app_aforo/models/Establecimiento.dart';
import 'package:app_aforo/screens/menu/editar_establecimiento_step2_screen.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/opcion.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditarEstablecimientoStep1Screen extends StatefulWidget {
  static const String id = 'editar_establecimiento_step1_screen';
  final Establecimiento establecimiento;

  const EditarEstablecimientoStep1Screen({required this.establecimiento});

  @override
  _EditarEstablecimientoStep1ScreenState createState() =>
      _EditarEstablecimientoStep1ScreenState();
}

class _EditarEstablecimientoStep1ScreenState
    extends State<EditarEstablecimientoStep1Screen> {
  late Establecimiento establecimiento;
  late List<Opcion> tiposEstablecimiento = [];
  late TextEditingController controllerNombre;
  late TextEditingController controllerRUC;
  late TextEditingController controllerDireccion;
  late TextEditingController controllerAforoMaximo;

  @override
  void initState() {
    super.initState();
    establecimiento = widget.establecimiento;
    tiposEstablecimiento.add(Opcion(texto: 'Banco', valor: 'Banco'));
    tiposEstablecimiento
        .add(Opcion(texto: 'Centro Comercial', valor: 'CentroComercial'));
    tiposEstablecimiento
        .add(Opcion(texto: 'Restaurante', valor: 'Restaurante'));
    tiposEstablecimiento.add(Opcion(texto: 'Tienda', valor: 'Tienda'));
    controllerNombre = TextEditingController(text: establecimiento.nombre!);
    controllerRUC = TextEditingController(text: establecimiento.ruc!);
    controllerDireccion =
        TextEditingController(text: establecimiento.direccion!);
    controllerAforoMaximo =
        TextEditingController(text: establecimiento.aforoMaximo!.toString());
  }

  bool esFormularioValido() {
    return (establecimiento.nombre != null &&
        establecimiento.nombre!.isNotEmpty &&
        establecimiento.ruc != null &&
        establecimiento.ruc!.isNotEmpty &&
        establecimiento.direccion != null &&
        establecimiento.direccion!.isNotEmpty &&
        establecimiento.tipo != null &&
        establecimiento.tipo!.isNotEmpty &&
        establecimiento.aforoMaximo != null &&
        establecimiento.aforoMaximo! > 0 &&
        validarNombre() == null &&
        validarRUC() == null &&
        validarDireccion() == null &&
        validarAforoMaximo() == null);
  }

  String? validarNombre() {
    String? mensajeError;
    if (establecimiento.nombre != null) {
      if (establecimiento.nombre!.length > 50) {
        mensajeError = 'Máximo 50 caracteres';
      }
    }
    return mensajeError;
  }

  String? validarRUC() {
    String? mensajeError;
    if (establecimiento.ruc != null) {
      if (establecimiento.ruc!.length != 11) {
        mensajeError = 'Debe tener 11 caracteres';
      }
    }
    return mensajeError;
  }

  String? validarDireccion() {
    String? mensajeError;
    if (establecimiento.direccion != null) {
      if (establecimiento.direccion!.length > 100) {
        mensajeError = 'Máximo 100 caracteres';
      }
    }
    return mensajeError;
  }

  String? validarAforoMaximo() {
    String? mensajeError;
    if (establecimiento.aforoMaximo != null) {
      if (establecimiento.aforoMaximo! == 0) {
        mensajeError = 'Valor mayor a 0';
      }
    }
    return mensajeError;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: AppBar(
        title: Text(
          'Editar establecimiento',
        ),
        backgroundColor: kGreenPrimaryColor,
      ),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 10,
              ),
              child: Column(
                children: [
                  Icon(
                    Icons.business,
                    color: kGreenPrimaryColor,
                    size: 130,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      border: Border.all(
                        color: kGreenPrimaryColor,
                        width: 1.0,
                      ),
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 30,
                        vertical: 20,
                      ),
                      child: Column(
                        children: [
                          RoundedTextFieldWithValidations(
                            suffixIcon: Icons.business,
                            texto: 'Nombre',
                            onChange: (value) {
                              setState(() {
                                establecimiento =
                                    establecimiento.copyWith(nombre: value);
                              });
                            },
                            obscureText: false,
                            inputType: TextInputType.text,
                            borderColor: kGreenPrimaryColor,
                            borderRadius: 30,
                            onTap: () {},
                            controller: controllerNombre,
                            textoError: validarNombre(),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          RoundedTextFieldWithValidations(
                            suffixIcon: Icons.business,
                            texto: 'RUC',
                            onChange: (value) {
                              setState(() {
                                establecimiento =
                                    establecimiento.copyWith(ruc: value);
                              });
                            },
                            obscureText: false,
                            inputType: TextInputType.number,
                            borderColor: kGreenPrimaryColor,
                            borderRadius: 30,
                            onTap: () {},
                            controller: controllerRUC,
                            textoError: validarRUC(),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          RoundedTextFieldWithValidations(
                            suffixIcon: Icons.pin_drop_sharp,
                            texto: 'Dirección',
                            onChange: (value) {
                              setState(() {
                                establecimiento =
                                    establecimiento.copyWith(direccion: value);
                              });
                            },
                            obscureText: false,
                            inputType: TextInputType.text,
                            borderColor: kGreenPrimaryColor,
                            borderRadius: 30,
                            onTap: () {},
                            controller: controllerDireccion,
                            textoError: validarDireccion(),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          RoundedTextFieldWithValidations(
                            suffixIcon: Icons.group_rounded,
                            texto: 'Aforo máximo',
                            onChange: (value) {
                              setState(() {
                                (value == '')
                                    ? establecimiento =
                                        establecimiento.copyWith(aforoMaximo: 0)
                                    : establecimiento =
                                        establecimiento.copyWith(
                                            aforoMaximo: int.parse(value));
                              });
                            },
                            obscureText: false,
                            inputType: TextInputType.number,
                            borderColor: kGreenPrimaryColor,
                            borderRadius: 30,
                            onTap: () {},
                            controller: controllerAforoMaximo,
                            esNumeroEntero: true,
                            textoError: validarAforoMaximo(),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          RoundedDropDownButton(
                            colorBorde: kGreenPrimaryColor,
                            lista: tiposEstablecimiento,
                            value: establecimiento.tipo!,
                            borderRadius: 30,
                            onChange: (tipo) {
                              setState(() {
                                establecimiento =
                                    establecimiento.copyWith(tipo: tipo);
                              });
                            },
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: RoundedButton(
                              texto: 'Seleccionar Ubicación',
                              color: esFormularioValido()
                                  ? kGreenPrimaryColor
                                  : kLightGrayPrimaryColor,
                              onPressed: !esFormularioValido()
                                  ? null
                                  : () async {
                                      FocusScope.of(context).unfocus();
                                      var route = CupertinoPageRoute(
                                        builder: (BuildContext context) =>
                                            EditarEstablecimientoStep2Screen(
                                          establecimiento: establecimiento,
                                        ),
                                      );
                                      await Navigator.of(context).push(route);
                                    },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
