import 'package:app_aforo/components/rounded_button.dart';
import 'package:app_aforo/models/Direccion.dart';
import 'package:app_aforo/models/Establecimiento.dart';
import 'package:app_aforo/screens/menu/menu_screen.dart';
import 'package:app_aforo/services/establecimiento_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
import 'package:app_aforo/utils/direction_search.dart';

class EditarEstablecimientoStep2Screen extends StatefulWidget {
  static const String id = 'editar_establecimiento_step2_screen';
  final Establecimiento? establecimiento;

  const EditarEstablecimientoStep2Screen({this.establecimiento});
  @override
  _EditarEstablecimientoStep2ScreenState createState() =>
      _EditarEstablecimientoStep2ScreenState();
}

class _EditarEstablecimientoStep2ScreenState
    extends State<EditarEstablecimientoStep2Screen> {
  List<Result> historial = [];
  Completer<GoogleMapController> _controller = Completer();
  LatLng target = LatLng(-12.04318, -77.02824);

  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _establecimientoService = EstablecimientoService.instance;
  late Establecimiento establecimiento;
  List<Marker> markerEstablecimiento = [];
  bool estaCargando = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      establecimiento = widget.establecimiento!;
      markerEstablecimiento.add(
        Marker(
          markerId: MarkerId(
              LatLng(establecimiento.latitud!, establecimiento.longitud!)
                  .toString()),
          position: LatLng(establecimiento.latitud!, establecimiento.longitud!),
        ),
      );
    });
    moverMapaWithLatLng(LatLng(
      establecimiento.latitud!,
      establecimiento.longitud!,
    ));
  }

  posicionarMarker(LatLng latLng) {
    setState(() {
      markerEstablecimiento = [];
      markerEstablecimiento.add(
        Marker(
          markerId: MarkerId(latLng.toString()),
          position: latLng,
        ),
      );
      establecimiento = establecimiento.copyWith(
        latitud: latLng.latitude,
        longitud: latLng.longitude,
      );
    });
  }

  Future<void> moverMapa() async {
    var controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newLatLng(target));
  }

  Future<void> moverMapaWithLatLng(LatLng latLng) async {
    var controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newLatLng(latLng));
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text('Editar establecimiento'),
          backgroundColor: kGreenPrimaryColor,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () async {
                Result? direccion = await showSearch(
                  context: context,
                  delegate: DirectionSearchDelegate(this.historial),
                );
                if (direccion != null) {
                  setState(() {
                    target = LatLng(
                      direccion.geometry.location.lat,
                      direccion.geometry.location.lng,
                    );
                    historial.insert(0, direccion);
                    historial = historial.toSet().toList();
                  });
                  moverMapa();
                }
              },
            )
          ],
        ),
        body: Stack(
          children: [
            SafeArea(
              child: Column(
                children: [
                  Expanded(
                    child: GoogleMap(
                      markers: Set.from(markerEstablecimiento),
                      onTap: posicionarMarker,
                      initialCameraPosition: CameraPosition(
                        zoom: 15.0,
                        target: target,
                      ),
                      onMapCreated: (GoogleMapController controller) {
                        _controller.complete(controller);
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 20,
                    ),
                    child: RoundedButton(
                      texto: 'Guardar',
                      color: markerEstablecimiento.isEmpty
                          ? kLightGrayPrimaryColor
                          : kGreenPrimaryColor,
                      onPressed: markerEstablecimiento.isEmpty
                          ? null
                          : () {
                              actualizar();
                            },
                    ),
                  ),
                ],
              ),
            ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Future<void> actualizar() async {
    setState(() => estaCargando = true);
    try {
      await _establecimientoService.create(establecimiento);
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text('Se actualizó el establecimiento correctamente'),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
      _establecimientoService.actualizarAforosEstablecimiento(establecimiento);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => MenuScreen(indexPage: 2)),
          (Route<dynamic> route) => false);
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }
}
