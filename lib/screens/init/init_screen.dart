import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_storage_s3/amplify_storage_s3.dart';
import 'package:app_aforo/screens/menu/menu_screen.dart';
import 'package:app_aforo/screens/user/login_screen.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:provider/provider.dart';
import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_datastore/amplify_datastore.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:app_aforo/amplifyconfiguration.dart';
import 'package:app_aforo/models/ModelProvider.dart';

class InitScreen extends StatefulWidget {
  static const String id = 'init_screen';
  @override
  _InitScreenState createState() => _InitScreenState();
}

class _InitScreenState extends State<InitScreen> {
  Future<void> _configureAmplify() async {
    try {
      await Future.wait([
        Amplify.addPlugins([
          AmplifyAuthCognito(),
          AmplifyStorageS3(),
          AmplifyDataStore(modelProvider: ModelProvider.instance),
          AmplifyAPI()
        ]),
      ]);
      await Amplify.configure(amplifyconfig);
    } catch (e) {
      print(kMensajeErrorGenerico);
    }
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(
      Duration(seconds: 3),
      () async {
        await _configureAmplify();
        var existeUsuario =
            await Provider.of<UsuarioState>(context, listen: false)
                .existUsuario();
        if (existeUsuario) {
          await Navigator.pushReplacementNamed(context, MenuScreen.id);
        } else {
          await Navigator.pushReplacementNamed(context, LoginScreen.id);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreenPrimaryColor,
      body: Center(
        child: GlowingProgressIndicator(
          child: Icon(
            Icons.business_rounded,
            color: Colors.white,
            size: 200,
          ),
        ),
      ),
    );
  }
}
