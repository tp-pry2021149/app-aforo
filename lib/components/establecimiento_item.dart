import 'package:app_aforo/models/Establecimiento.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EstablecimientoItem extends StatefulWidget {
  final Establecimiento establecimiento;
  final bool esEditable;
  final Function onTapEliminar;
  final Function onTapEditar;

  const EstablecimientoItem({
    required this.establecimiento,
    required this.esEditable,
    required this.onTapEliminar,
    required this.onTapEditar,
  });
  @override
  _EstablecimientoItemState createState() => _EstablecimientoItemState();
}

class _EstablecimientoItemState extends State<EstablecimientoItem> {
  late DateTime fechaHoraRegistro;

  @override
  Widget build(BuildContext context) {
    if (widget.establecimiento.fechaRegistroUltimoAforo != null) {
      fechaHoraRegistro = DateTime.fromMillisecondsSinceEpoch(
          widget.establecimiento.fechaRegistroUltimoAforo!.toSeconds() * 1000);
    }
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Material(
            elevation: 5,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
            color: kLightGreenPrimaryColor,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: (widget.esEditable)
                        ? BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(0),
                            bottomRight: Radius.circular(0),
                          )
                        : BorderRadius.all(Radius.circular(20)),
                    color: Colors.white,
                    border: Border.all(color: kGreenPrimaryColor),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 15,
                      ),
                      (widget.establecimiento.ultimoAforoRegistrado != null &&
                              widget.establecimiento.fechaRegistroUltimoAforo !=
                                  null)
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(10)),
                                      color: kRedPrimaryColor,
                                      border:
                                          Border.all(color: kRedPrimaryColor),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(5),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  '${widget.establecimiento.ultimoAforoRegistrado}/${widget.establecimiento.aforoMaximo}',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                Text(
                                                  '${((widget.establecimiento.ultimoAforoRegistrado! / widget.establecimiento.aforoMaximo!) * 100).toStringAsFixed(2)}%',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(5),
                                            child: Icon(
                                              Icons.groups,
                                              size: 35,
                                              color: Colors.white,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.schedule,
                                      size: 20,
                                      color: Colors.black,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Column(
                                      children: [
                                        Text(
                                          DateFormat('dd/MM/yyyy')
                                              .format(fechaHoraRegistro),
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 13,
                                          ),
                                        ),
                                        Text(
                                          DateFormat('hh:mm a')
                                              .format(fechaHoraRegistro),
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 13,
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )
                          : Padding(
                              padding: const EdgeInsets.all(10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'No hay aforo registrado',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                    ],
                  ),
                ),
                if (widget.esEditable)
                  Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(0),
                                topRight: Radius.circular(0),
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(0),
                              ),
                              border: Border.all(color: kGreenPrimaryColor),
                              color: kLightGreenPrimaryColor,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(4),
                              child: Icon(
                                Icons.delete,
                                color: Colors.white,
                                size: 15,
                              ),
                            ),
                          ),
                          onTap: () {
                            if (widget.esEditable) {
                              widget.onTapEliminar();
                            }
                          },
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(0),
                                topRight: Radius.circular(0),
                                bottomLeft: Radius.circular(0),
                                bottomRight: Radius.circular(20),
                              ),
                              color: kLightGreenPrimaryColor,
                              border: Border.all(color: kGreenPrimaryColor),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(4),
                              child: Icon(
                                Icons.edit,
                                color: Colors.white,
                                size: 15,
                              ),
                            ),
                          ),
                          onTap: () {
                            if (widget.esEditable) {
                              widget.onTapEditar();
                            }
                          },
                        ),
                      ),
                    ],
                  ),
              ],
            ),
          ),
          Positioned(
            top: -15,
            right: 0,
            left: 0,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                  color: kGreenPrimaryColor,
                  border: Border.all(color: kGreenPrimaryColor),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Text(
                    '${widget.establecimiento.nombre}',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
