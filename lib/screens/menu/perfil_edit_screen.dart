import 'dart:io';
import 'package:app_aforo/components/rounded_button.dart';
import 'package:app_aforo/models/Usuario.dart';
import 'package:app_aforo/services/archivo_service.dart';
import 'package:app_aforo/services/usuario_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:app_aforo/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PerfilEditScreen extends StatefulWidget {
  static const String id = 'perfil_edit_screen';

  @override
  _PerfilEditScreenState createState() => _PerfilEditScreenState();
}

class _PerfilEditScreenState extends State<PerfilEditScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _usuarioService = UsuarioService.instance;
  final _archivoService = ArchivoService.instance;
  late Usuario usuario;
  late TextEditingController controllerNombres;
  late TextEditingController controllerApellidos;
  late TextEditingController controllerNombreUsuario;
  late TextEditingController controllerCorreoElectronico;
  late File image;
  bool estaCargando = false;
  bool estaCargandoImagen = false;

  @override
  void initState() {
    super.initState();
    usuario = Provider.of<UsuarioState>(context, listen: false).usuario;
    controllerNombres = TextEditingController(text: usuario.nombres!);
    controllerApellidos = TextEditingController(text: usuario.apellidos!);
    controllerNombreUsuario =
        TextEditingController(text: usuario.nombreUsuario!);
    controllerCorreoElectronico =
        TextEditingController(text: usuario.correoElectronico!);
  }

  Future<void> cambiarFotoPerfil(File image) async {
    setState(() => estaCargandoImagen = true);
    try {
      final key = usuario.id + '${DateTime.now()}' + '.png';
      final fileKey = await _archivoService.uploadImage(image: image, key: key);
      setState(() {
        usuario = usuario.copyWith(urlImagen: kUrlBase + fileKey);
      });
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      Future.delayed(Duration(seconds: 3), () {
        setState(() => estaCargandoImagen = false);
      });
    }
  }

  Future<void> actualizarUsuario() async {
    setState(() => estaCargando = true);
    try {
      FocusScope.of(context).unfocus();
      await _usuarioService.create(usuario);
      await Provider.of<UsuarioState>(context, listen: false).deleteUsuario();
      await Provider.of<UsuarioState>(context, listen: false)
          .setUsuario(usuario);
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text('Datos actualizados correctamente'),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
      Navigator.pop(context);
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text('Editar perfil'),
          backgroundColor: kGreenPrimaryColor,
        ),
        body: Stack(
          children: [
            SafeArea(
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Material(
                        elevation: 5,
                        borderRadius: BorderRadius.all(
                          Radius.circular(60),
                        ),
                        color: kBackgroundColor,
                        child: Hero(
                          tag: 'imagenUsuario',
                          child: Container(
                            width: 120,
                            height: 120,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  '${usuario.urlImagen}',
                                ),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.all(
                                Radius.circular(60),
                              ),
                              border: Border.all(
                                color: kGreenPrimaryColor,
                                width: 2,
                              ),
                            ),
                            child: (estaCargandoImagen)
                                ? CircularProgressIndicator(
                                    color: kGreenPrimaryColor,
                                    strokeWidth: 4,
                                  )
                                : null,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: GestureDetector(
                          onTap: () {
                            Utils.showDialogImagePicker(context, (file) async {
                              setState(() => image = File(file));
                              if (image.path != '') {
                                cambiarFotoPerfil(image);
                              }
                            });
                          },
                          child: Text(
                            'Cambiar foto de perfil',
                            style: TextStyle(
                              fontSize: 15,
                              color: kGreenPrimaryColor,
                            ),
                          ),
                        ),
                      ),
                      Divider(
                        color: kGreenPrimaryColor,
                        thickness: 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 100,
                                  child: Text(
                                    'Nombres',
                                    maxLines: 2,
                                  ),
                                ),
                                Expanded(
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                      border: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                    ),
                                    controller: controllerNombres,
                                    onChanged: (String? value) {
                                      setState(() {
                                        usuario =
                                            usuario.copyWith(nombres: value);
                                      });
                                    },
                                  ),
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 100,
                                  child: Text(
                                    'Apellidos',
                                    maxLines: 2,
                                  ),
                                ),
                                Expanded(
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                      border: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                    ),
                                    controller: controllerApellidos,
                                    onChanged: (String? value) {
                                      setState(() {
                                        usuario =
                                            usuario.copyWith(apellidos: value);
                                      });
                                    },
                                  ),
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 100,
                                  child: Text(
                                    'Nombre de usuario',
                                    maxLines: 2,
                                  ),
                                ),
                                Expanded(
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                      border: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                    ),
                                    controller: controllerNombreUsuario,
                                    readOnly: true,
                                    onChanged: (String? value) {
                                      setState(() {
                                        usuario = usuario.copyWith(
                                            nombreUsuario: value);
                                      });
                                    },
                                  ),
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 100,
                                  child: Text(
                                    'Correo electrónico',
                                    maxLines: 2,
                                  ),
                                ),
                                Expanded(
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                      border: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: kGreenPrimaryColor),
                                      ),
                                    ),
                                    controller: controllerCorreoElectronico,
                                    onChanged: (String? value) {
                                      setState(() {
                                        usuario = usuario.copyWith(
                                            correoElectronico: value);
                                      });
                                    },
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Divider(
                        color: kGreenPrimaryColor,
                        thickness: 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 60,
                          vertical: 20,
                        ),
                        child: RoundedButton(
                          texto: 'Guardar',
                          color: kGreenPrimaryColor,
                          onPressed: () async {
                            actualizarUsuario();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
