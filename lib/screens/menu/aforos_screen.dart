import 'package:app_aforo/components/aforo_item.dart';
import 'package:app_aforo/components/eliminar_aforo_dialog.dart';
import 'package:app_aforo/components/rounded_text_field.dart';
import 'package:app_aforo/models/Aforo.dart';
import 'package:app_aforo/screens/menu/register_aforo_screen.dart';
import 'package:app_aforo/services/establecimiento_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AforosScreen extends StatefulWidget {
  static const String id = 'aforos_screen';
  final String? establecimientoId;

  const AforosScreen({this.establecimientoId});

  @override
  _AforosScreenState createState() => _AforosScreenState();
}

class _AforosScreenState extends State<AforosScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _establecimientoService = EstablecimientoService.instance;
  bool estaCargando = false;
  bool hayAforos = true;
  late List<Aforo> aforos;
  DateTime fecha = DateTime.now();
  late TextEditingController controllerFecha;

  @override
  void initState() {
    super.initState();
    controllerFecha = TextEditingController(
      text: DateFormat('dd/MM/yyyy').format(fecha),
    );
    obtenerAforos(fecha);
  }

  Future<void> obtenerAforos(DateTime fecha) async {
    setState(() => estaCargando = true);
    try {
      aforos = [];
      final fechaSinHora = DateTime(fecha.year, fecha.month, fecha.day, 0, 0);
      aforos = await _establecimientoService.getAforos(
        establecimientoId: widget.establecimientoId,
        fecha: fechaSinHora,
      );
      aforos.sort(
        (a, b) => DateTime.fromMillisecondsSinceEpoch(
                b.fechaHoraRegistro!.toSeconds() * 1000)
            .compareTo(DateTime.fromMillisecondsSinceEpoch(
                a.fechaHoraRegistro!.toSeconds() * 1000)),
      );
      if (aforos.length == 0) {
        setState(() {
          hayAforos = false;
        });
      } else {
        setState(() {
          hayAforos = true;
        });
      }
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  Future<void> selectDate(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2200),
      helpText: 'Fecha',
    );

    if (picked != null) {
      setState(() {
        fecha = picked;
        controllerFecha.text = DateFormat('dd/MM/yyyy').format(picked);
        obtenerAforos(fecha);
      });
    }
  }

  Future<bool?> mostrarDialogoEliminar(BuildContext context) async {
    return await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (_) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
        ),
        contentPadding: EdgeInsets.all(0),
        content: EliminarAforoDialog(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text('Aforos'),
          backgroundColor: kGreenPrimaryColor,
        ),
        body: Stack(
          children: [
            SafeArea(
              child: SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 40,
                          vertical: 20,
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: kGreenPrimaryColor,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(
                                20,
                              ),
                            ),
                            color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 50,
                              vertical: 15,
                            ),
                            child: RoundedTextField(
                              texto: "fecha",
                              inputType: TextInputType.datetime,
                              obscureText: false,
                              readOnly: true,
                              borderColor: kGreenPrimaryColor,
                              borderRadius: 30.0,
                              controller: controllerFecha,
                              onTap: () {
                                selectDate(context);
                              },
                            ),
                          ),
                        ),
                      ),
                      ListView.builder(
                        itemCount: aforos.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return AforoItem(
                              aforo: aforos[index],
                              eliminar: () async {
                                bool? respuesta =
                                    await mostrarDialogoEliminar(context);
                                if (respuesta != null) {
                                  await _establecimientoService
                                      .eliminarAforo(aforos[index]);
                                  setState(() {
                                    obtenerAforos(fecha);
                                  });
                                }
                              });
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (!hayAforos)
              Container(
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.sentiment_dissatisfied,
                        size: 50,
                        color: kGreenPrimaryColor,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'El ${controllerFecha.text} no\nregistró ningún aforo',
                        style: TextStyle(fontSize: 16),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: kGreenPrimaryColor,
          onPressed: () async {
            var route = CupertinoPageRoute(
              builder: (BuildContext context) => RegisterAforoScreen(
                establecimientoId: widget.establecimientoId,
              ),
            );
            await Navigator.of(context).push(route);
            setState(() {
              obtenerAforos(fecha);
            });
          },
          child: Icon(
            Icons.add,
            size: 24,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
