class CustomException implements Exception {
  final String mensaje;
  CustomException(this.mensaje);

  String toString() {
    return "$mensaje";
  }
}
