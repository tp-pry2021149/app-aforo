import 'dart:async';
import 'package:app_aforo/models/Direccion.dart';
import 'package:app_aforo/models/Establecimiento.dart';
import 'package:app_aforo/screens/menu/establecimiento_screen.dart';
import 'package:app_aforo/services/establecimiento_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:app_aforo/utils/direction_search.dart';

class MapaScreen extends StatefulWidget {
  static const String id = 'mapa_screen';

  @override
  _MapaScreenState createState() => _MapaScreenState();
}

class _MapaScreenState extends State<MapaScreen> {
  List<Result> historial = [];
  Completer<GoogleMapController> _controller = Completer();
  LatLng target = LatLng(-12.04318, -77.02824);

  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _establecimientoService = EstablecimientoService.instance;
  Set<Marker> establecimientosMarkers = {};
  late List<Establecimiento> establecimientos = [];
  bool estaCargando = false;

  @override
  void initState() {
    super.initState();
    obtenerEstablecimientos();
  }

  Future<void> obtenerEstablecimientos() async {
    setState(() => estaCargando = true);
    try {
      establecimientos = await _establecimientoService.getEstablecimientos();
      if (establecimientos.length > 0) {
        establecimientos.forEach((establecimiento) {
          establecimientosMarkers.add(
            Marker(
              markerId: MarkerId(establecimiento.id),
              position:
                  LatLng(establecimiento.latitud!, establecimiento.longitud!),
              infoWindow: InfoWindow(
                title: establecimiento.nombre,
                snippet: (establecimiento.fechaRegistroUltimoAforo != null &&
                        establecimiento.ultimoAforoRegistrado != null)
                    ? 'Aforo: ${((establecimiento.ultimoAforoRegistrado! / establecimiento.aforoMaximo!) * 100).toStringAsFixed(2)}%'
                    : 'No hay aforo registrado',
                onTap: () async {
                  if (establecimiento.fechaRegistroUltimoAforo != null &&
                      establecimiento.ultimoAforoRegistrado != null) {
                    var route = CupertinoPageRoute(
                      builder: (BuildContext context) => EstablecimientoScreen(
                        establecimientoId: establecimiento.id,
                      ),
                    );
                    await Navigator.of(context).push(route);
                    setState(() {
                      obtenerEstablecimientos();
                    });
                  }
                },
              ),
            ),
          );
        });
      }
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text('Mapa'),
          backgroundColor: kGreenPrimaryColor,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () async {
                Result? direccion = await showSearch(
                  context: context,
                  delegate: DirectionSearchDelegate(this.historial),
                );
                if (direccion != null) {
                  setState(() {
                    target = LatLng(
                      direccion.geometry.location.lat,
                      direccion.geometry.location.lng,
                    );
                    historial.insert(0, direccion);
                    historial = historial.toSet().toList();
                  });
                  moverMapa();
                }
              },
            )
          ],
        ),
        body: Stack(
          children: [
            SafeArea(
              child: GoogleMap(
                markers: establecimientosMarkers,
                initialCameraPosition: CameraPosition(
                  zoom: 15.0,
                  target: target,
                ),
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                },
              ),
            ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Future<void> moverMapa() async {
    var controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newLatLng(target));
  }
}
