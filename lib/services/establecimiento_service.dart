import 'package:amplify_datastore/amplify_datastore.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:app_aforo/models/Aforo.dart';
import 'package:app_aforo/models/Establecimiento.dart';
import 'package:app_aforo/models/ModelProvider.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'dart:developer' as dev;

class EstablecimientoService {
  static EstablecimientoService _instance =
      EstablecimientoService._privateConstructor();
  static EstablecimientoService get instance => _instance;
  EstablecimientoService._privateConstructor();

  Future<void> create(Establecimiento establecimiento) async {
    dev.log('Creando establecimiento en DynamoDb $establecimiento',
        name: 'EstablecimientoService.create');
    try {
      await Amplify.DataStore.save(establecimiento);
    } catch (e) {
      dev.log(e.toString(), name: 'EstablecimientoService.create');
      throw CustomException("Ocurrió un error al crear establecimiento");
    }
  }

  Future<List<Establecimiento>> getEstablecimientos() async {
    dev.log('Obteniendo establecimientos',
        name: 'EstablecimientoService.getEstablecimientos');
    try {
      List<Establecimiento> establecimientos =
          await Amplify.DataStore.query(Establecimiento.classType);
      return establecimientos;
    } catch (e) {
      dev.log(e.toString(), name: 'EstablecimientoService.getEstablecimientos');
      throw CustomException("Ocurrió un error al obtener los establecimientos");
    }
  }

  Future<List<Establecimiento>> getEstablecimientosByUsuarioId(
      {String? usuarioId}) async {
    dev.log('Obteniendo establecimientos del usuario con id: $usuarioId',
        name: 'EstablecimientoService.getEstablecimientosByUsuarioId');
    try {
      List<Establecimiento> establecimientos = await Amplify.DataStore.query(
        Establecimiento.classType,
        where: Establecimiento.USUARIOID.eq(usuarioId),
      );
      return establecimientos;
    } catch (e) {
      dev.log(e.toString(),
          name: 'EstablecimientoService.getEstablecimientosByUsuarioId');
      throw CustomException(
          "Ocurrió un error al obtener los establecimientos del usuario");
    }
  }

  Future<Establecimiento> getEstablecimientoById(
      {String? establecimientoId}) async {
    dev.log('Obteniendo establecimiento con id: $establecimientoId',
        name: 'EstablecimientoService.getEstablecimientoById');
    try {
      List<Establecimiento> establecimientos = await Amplify.DataStore.query(
        Establecimiento.classType,
        where: Establecimiento.ID.eq(establecimientoId),
      );
      if (establecimientos.isEmpty) {
        throw 'No se encontró el establecimiento';
      }
      return establecimientos.first;
    } catch (e) {
      dev.log(e.toString(),
          name: 'EstablecimientoService.getEstablecimientoById');
      throw CustomException("Ocurrió un error al obtener establecimiento");
    }
  }

  Future<List<Aforo>> getAforos(
      {String? establecimientoId, DateTime? fecha}) async {
    dev.log(
        'Obteniendo aforos de la fecha: $fecha  del establecimiento con id: $establecimientoId',
        name: 'EstablecimientoService.getAforos');
    try {
      List<Aforo> aforos = await Amplify.DataStore.query(
        Aforo.classType,
        where: Aforo.ESTABLECIMIENTOID.eq(establecimientoId).and(
              Aforo.FECHAHORAREGISTRO.between(
                TemporalTimestamp(fecha!).toSeconds(),
                TemporalTimestamp(fecha.add(Duration(days: 1))).toSeconds(),
              ),
            ),
      );
      return aforos;
    } catch (e) {
      dev.log(e.toString(), name: 'EstablecimientoService.getAforos');
      throw CustomException(
          "Ocurrió un error al obtener aforos del establecimiento");
    }
  }

  Future<void> createAforo(Aforo aforo) async {
    dev.log('Creando aforo en DynamoDb $aforo',
        name: 'EstablecimientoService.createAforo');
    try {
      await Amplify.DataStore.save(aforo);
    } catch (e) {
      dev.log(e.toString(), name: 'EstablecimientoService.createAforo');
      throw CustomException("Ocurrió un error al crear aforo");
    }
  }

  Future<void> eliminarAforo(Aforo aforo) async {
    dev.log('Eliminando aforo de DynamoDb $aforo',
        name: 'EstablecimientoService.eliminarAforo');
    try {
      await Amplify.DataStore.delete(aforo);
      actualizarUltimoAforoEstablecimiento(aforo);
    } catch (e) {
      dev.log(e.toString(), name: 'EstablecimientoService.eliminarAforo');
      throw CustomException("Ocurrió un error al eliminar aforo");
    }
  }

  Future<void> eliminarEstablecimiento(Establecimiento establecimiento) async {
    dev.log('Eliminando establecimiento de DynamoDb $establecimiento',
        name: 'EstablecimientoService.eliminarEstablecimiento');
    try {
      await Amplify.DataStore.delete(establecimiento);
      // TODO ELIMINAR LOS AFOROS DEL ESTABLECIMIENTO
      // ELIMINAR DE LOS USUARIO QUE LO TIENE COMO FAVORITO
      List<Usuario> usuarios = await Amplify.DataStore.query(Usuario.classType);
      usuarios.forEach((usuario) {
        if (usuario.establecimientosFavoritos != null) {
          var establecimientosFavoritos =
              usuario.establecimientosFavoritos!.toList();
          establecimientosFavoritos.removeWhere(
              (establecimientoId) => establecimientoId == establecimiento.id);
          usuario = usuario.copyWith(
              establecimientosFavoritos: establecimientosFavoritos);
          Amplify.DataStore.save(usuario);
        }
      });
    } catch (e) {
      dev.log(e.toString(),
          name: 'EstablecimientoService.eliminarEstablecimiento');
      throw CustomException("Ocurrió un error al eliminar establecimiento");
    }
  }

  // TODO USAR FUNCTIONS
  Future<void> actualizarAforosEstablecimiento(
      Establecimiento establecimiento) async {
    // Listar aforos del establecimiento
    List<Aforo> aforos = await Amplify.DataStore.query(Aforo.classType,
        where: Aforo.ESTABLECIMIENTOID.eq(establecimiento.id));
    aforos.forEach((aforo) {
      Aforo aforoActualizado = Aforo(
          aforoMaximoEstablecimiento: establecimiento.aforoMaximo,
          cantidad: aforo.cantidad,
          establecimientoId: establecimiento.id,
          fechaHoraRegistro: aforo.fechaHoraRegistro,
          nombreEstablecimiento: establecimiento.nombre,
          id: aforo.id,
          usuarioId: aforo.usuarioId);
      Amplify.DataStore.save(aforoActualizado);
    });
  }

  // TODO USAR FUNCTIONS
  Future<void> actualizarUltimoAforoEstablecimiento(Aforo aforo) async {
    // Listar aforos del establecimiento
    List<Aforo> aforos = await Amplify.DataStore.query(Aforo.classType,
        where: Aforo.ESTABLECIMIENTOID.eq(aforo.establecimientoId));

    if (aforos.length > 0) {
      // Ordenar del mas reciente
      aforos.sort(
        (a, b) => DateTime.fromMillisecondsSinceEpoch(
                b.fechaHoraRegistro!.toSeconds() * 1000)
            .compareTo(DateTime.fromMillisecondsSinceEpoch(
                a.fechaHoraRegistro!.toSeconds() * 1000)),
      );
      // Obtener establecimiento
      List<Establecimiento> establecimientos = await Amplify.DataStore.query(
        Establecimiento.classType,
        where: Establecimiento.ID.eq(aforo.establecimientoId),
      );
      await Amplify.DataStore.save(establecimientos.first.copyWith(
          ultimoAforoRegistrado: aforos.first.cantidad,
          fechaRegistroUltimoAforo: aforos.first.fechaHoraRegistro));
    } else {
      // Colocar null en el aforo del establecimiento
      List<Establecimiento> establecimientos = await Amplify.DataStore.query(
        Establecimiento.classType,
        where: Establecimiento.ID.eq(aforo.establecimientoId),
      );
      Establecimiento establecimiento = Establecimiento(
        aforoMaximo: establecimientos.first.aforoMaximo,
        direccion: establecimientos.first.direccion,
        id: establecimientos.first.id,
        latitud: establecimientos.first.latitud,
        longitud: establecimientos.first.longitud,
        nombre: establecimientos.first.nombre,
        ruc: establecimientos.first.ruc,
        tipo: establecimientos.first.tipo,
        usuarioId: establecimientos.first.usuarioId,
      );
      await Amplify.DataStore.save(establecimiento);
    }
  }
}
