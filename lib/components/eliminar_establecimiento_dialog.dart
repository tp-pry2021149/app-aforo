import 'package:app_aforo/components/rounded_button.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:flutter/material.dart';

class EliminarEstablecimientoDialog extends StatelessWidget {
  const EliminarEstablecimientoDialog();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 10),
              child: Text(
                '¿Deseas eliminar \n el establecimiento?',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.normal,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 30,
                vertical: 20,
              ),
              child: RoundedButton(
                texto: 'Eliminar',
                color: kGreenPrimaryColor,
                onPressed: () {
                  Navigator.pop(context, true);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
