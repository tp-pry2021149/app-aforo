/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// ignore_for_file: public_member_api_docs

import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Calificacion type in your schema. */
@immutable
class Calificacion extends Model {
  static const classType = const _CalificacionModelType();
  final String id;
  final TemporalTimestamp? _fechaHoraRegistro;
  final String? _comentario;
  final double? _puntaje;
  final String? _nombresUsuario;
  final String? _apellidosUsuario;
  final String? _urlImagenUsuario;
  final String? _usuarioId;
  final String? _establecimientoId;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  TemporalTimestamp? get fechaHoraRegistro {
    return _fechaHoraRegistro;
  }
  
  String? get comentario {
    return _comentario;
  }
  
  double? get puntaje {
    return _puntaje;
  }
  
  String? get nombresUsuario {
    return _nombresUsuario;
  }
  
  String? get apellidosUsuario {
    return _apellidosUsuario;
  }
  
  String? get urlImagenUsuario {
    return _urlImagenUsuario;
  }
  
  String? get usuarioId {
    return _usuarioId;
  }
  
  String? get establecimientoId {
    return _establecimientoId;
  }
  
  const Calificacion._internal({required this.id, fechaHoraRegistro, comentario, puntaje, nombresUsuario, apellidosUsuario, urlImagenUsuario, usuarioId, establecimientoId}): _fechaHoraRegistro = fechaHoraRegistro, _comentario = comentario, _puntaje = puntaje, _nombresUsuario = nombresUsuario, _apellidosUsuario = apellidosUsuario, _urlImagenUsuario = urlImagenUsuario, _usuarioId = usuarioId, _establecimientoId = establecimientoId;
  
  factory Calificacion({String? id, TemporalTimestamp? fechaHoraRegistro, String? comentario, double? puntaje, String? nombresUsuario, String? apellidosUsuario, String? urlImagenUsuario, String? usuarioId, String? establecimientoId}) {
    return Calificacion._internal(
      id: id == null ? UUID.getUUID() : id,
      fechaHoraRegistro: fechaHoraRegistro,
      comentario: comentario,
      puntaje: puntaje,
      nombresUsuario: nombresUsuario,
      apellidosUsuario: apellidosUsuario,
      urlImagenUsuario: urlImagenUsuario,
      usuarioId: usuarioId,
      establecimientoId: establecimientoId);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Calificacion &&
      id == other.id &&
      _fechaHoraRegistro == other._fechaHoraRegistro &&
      _comentario == other._comentario &&
      _puntaje == other._puntaje &&
      _nombresUsuario == other._nombresUsuario &&
      _apellidosUsuario == other._apellidosUsuario &&
      _urlImagenUsuario == other._urlImagenUsuario &&
      _usuarioId == other._usuarioId &&
      _establecimientoId == other._establecimientoId;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Calificacion {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("fechaHoraRegistro=" + (_fechaHoraRegistro != null ? _fechaHoraRegistro!.toString() : "null") + ", ");
    buffer.write("comentario=" + "$_comentario" + ", ");
    buffer.write("puntaje=" + (_puntaje != null ? _puntaje!.toString() : "null") + ", ");
    buffer.write("nombresUsuario=" + "$_nombresUsuario" + ", ");
    buffer.write("apellidosUsuario=" + "$_apellidosUsuario" + ", ");
    buffer.write("urlImagenUsuario=" + "$_urlImagenUsuario" + ", ");
    buffer.write("usuarioId=" + "$_usuarioId" + ", ");
    buffer.write("establecimientoId=" + "$_establecimientoId");
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Calificacion copyWith({String? id, TemporalTimestamp? fechaHoraRegistro, String? comentario, double? puntaje, String? nombresUsuario, String? apellidosUsuario, String? urlImagenUsuario, String? usuarioId, String? establecimientoId}) {
    return Calificacion(
      id: id ?? this.id,
      fechaHoraRegistro: fechaHoraRegistro ?? this.fechaHoraRegistro,
      comentario: comentario ?? this.comentario,
      puntaje: puntaje ?? this.puntaje,
      nombresUsuario: nombresUsuario ?? this.nombresUsuario,
      apellidosUsuario: apellidosUsuario ?? this.apellidosUsuario,
      urlImagenUsuario: urlImagenUsuario ?? this.urlImagenUsuario,
      usuarioId: usuarioId ?? this.usuarioId,
      establecimientoId: establecimientoId ?? this.establecimientoId);
  }
  
  Calificacion.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _fechaHoraRegistro = json['fechaHoraRegistro'] != null ? TemporalTimestamp.fromSeconds(json['fechaHoraRegistro']) : null,
      _comentario = json['comentario'],
      _puntaje = (json['puntaje'] as num?)?.toDouble(),
      _nombresUsuario = json['nombresUsuario'],
      _apellidosUsuario = json['apellidosUsuario'],
      _urlImagenUsuario = json['urlImagenUsuario'],
      _usuarioId = json['usuarioId'],
      _establecimientoId = json['establecimientoId'];
  
  Map<String, dynamic> toJson() => {
    'id': id, 'fechaHoraRegistro': _fechaHoraRegistro?.toSeconds(), 'comentario': _comentario, 'puntaje': _puntaje, 'nombresUsuario': _nombresUsuario, 'apellidosUsuario': _apellidosUsuario, 'urlImagenUsuario': _urlImagenUsuario, 'usuarioId': _usuarioId, 'establecimientoId': _establecimientoId
  };

  static final QueryField ID = QueryField(fieldName: "calificacion.id");
  static final QueryField FECHAHORAREGISTRO = QueryField(fieldName: "fechaHoraRegistro");
  static final QueryField COMENTARIO = QueryField(fieldName: "comentario");
  static final QueryField PUNTAJE = QueryField(fieldName: "puntaje");
  static final QueryField NOMBRESUSUARIO = QueryField(fieldName: "nombresUsuario");
  static final QueryField APELLIDOSUSUARIO = QueryField(fieldName: "apellidosUsuario");
  static final QueryField URLIMAGENUSUARIO = QueryField(fieldName: "urlImagenUsuario");
  static final QueryField USUARIOID = QueryField(fieldName: "usuarioId");
  static final QueryField ESTABLECIMIENTOID = QueryField(fieldName: "establecimientoId");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Calificacion";
    modelSchemaDefinition.pluralName = "Calificacions";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PUBLIC,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Calificacion.FECHAHORAREGISTRO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.timestamp)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Calificacion.COMENTARIO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Calificacion.PUNTAJE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Calificacion.NOMBRESUSUARIO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Calificacion.APELLIDOSUSUARIO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Calificacion.URLIMAGENUSUARIO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Calificacion.USUARIOID,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Calificacion.ESTABLECIMIENTOID,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
  });
}

class _CalificacionModelType extends ModelType<Calificacion> {
  const _CalificacionModelType();
  
  @override
  Calificacion fromJson(Map<String, dynamic> jsonData) {
    return Calificacion.fromJson(jsonData);
  }
}