import 'package:app_aforo/models/Direccion.dart';
import 'package:app_aforo/services/http_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'dart:developer' as dev;

class DirectionService {
  final _httpService = HttpService.instance;

  Future<Direccion> getDirectionByName(String name) async {
    try {
      dynamic data = await _httpService.get('/maps/api/geocode/json',
          params: {'address': name, 'key': keyAPI});
      return Direccion.fromMap(data);
    } catch (e) {
      dev.log(e.toString(), name: 'DirectionService.getDirectionByName');
      return new Direccion(results: [], status: 'ok');
    }
  }
}
