import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/opcion.dart';
import 'package:flutter/material.dart';

class RoundedDropDownButton extends StatelessWidget {
  final List<Opcion> lista;
  final Color colorBorde;
  final Function onChange;
  final String value;
  final double borderRadius;

  RoundedDropDownButton(
      {required this.lista,
      required this.colorBorde,
      required this.onChange,
      required this.value,
      required this.borderRadius});

  List<DropdownMenuItem<String>> getOpcionesDropdown() {
    List<DropdownMenuItem<String>> listaNueva = [];
    lista.forEach((opcion) {
      listaNueva.add(
        DropdownMenuItem(
          child: Center(
            child: Text(
              opcion.texto,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: kGreenPrimaryColor),
            ),
          ),
          value: opcion.valor,
        ),
      );
    });
    return listaNueva;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(borderRadius),
              border: Border.all(color: colorBorde, style: BorderStyle.solid),
            ),
            child: DropdownButtonHideUnderline(
              child: Padding(
                padding: const EdgeInsets.only(
                  top: 5,
                  bottom: 5,
                  left: 25,
                  right: 5,
                ),
                child: DropdownButton(
                  iconDisabledColor: kGreenPrimaryColor,
                  iconEnabledColor: kGreenPrimaryColor,
                  iconSize: 24,
                  isDense: true,
                  isExpanded: false,
                  elevation: 1,
                  value: value,
                  style: TextStyle(
                    color: kGrayPrimaryColor,
                    fontSize: 14,
                  ),
                  items: getOpcionesDropdown(),
                  onChanged: (value) => onChange(value),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
