import 'package:app_aforo/components/rounded_button.dart';
import 'package:app_aforo/components/rounded_text_field.dart';
import 'package:app_aforo/models/Usuario.dart';
import 'package:app_aforo/screens/menu/menu_screen.dart';
import 'package:app_aforo/screens/user/register_user_screen.dart';
import 'package:app_aforo/services/usuario_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  static const String id = 'login_screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _usuarioService = UsuarioService.instance;
  late String nombreUsuario;
  late String contrasenia;
  bool estaCargando = false;

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        body: Stack(
          children: [
            SafeArea(
              child: Stack(
                children: [
                  Positioned(
                    top: 0,
                    left: 0,
                    child: Image.asset(
                      kEsquinaPath,
                      width: 120,
                    ),
                  ),
                  Center(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          children: [
                            Text(
                              'App Aforo',
                              style: TextStyle(
                                color: kGreenPrimaryColor,
                                fontSize: 40,
                                fontFamily: schoolbell,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 20,
                              ),
                              child: Image.asset(
                                kLoginPath,
                                width: 200,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 40,
                                vertical: 10,
                              ),
                              child: RoundedTextField(
                                texto: 'Usuario',
                                inputType: TextInputType.text,
                                obscureText: false,
                                onChange: (value) {
                                  setState(() => nombreUsuario = value);
                                },
                                borderColor: kGreenPrimaryColor,
                                borderRadius: 30.0,
                                onTap: () {},
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 40,
                                vertical: 10,
                              ),
                              child: RoundedTextField(
                                texto: 'Contraseña',
                                inputType: TextInputType.text,
                                obscureText: true,
                                onChange: (value) {
                                  setState(() => contrasenia = value);
                                },
                                borderColor: kGreenPrimaryColor,
                                borderRadius: 30.0,
                                onTap: () {},
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 60,
                                vertical: 10,
                              ),
                              child: RoundedButton(
                                texto: 'Iniciar Sesión',
                                color: kGreenPrimaryColor,
                                onPressed: () {
                                  iniciarSesion();
                                },
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 60,
                              ),
                              child: RoundedButton(
                                texto: 'Registrar',
                                color: kLightGreenPrimaryColor,
                                onPressed: () {
                                  Navigator.of(context).push(CupertinoPageRoute(
                                      builder: (BuildContext context) {
                                    return RegisterUserScreen();
                                  }));
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if (estaCargando)
              Container(
                color: kBackgroundColor.withOpacity(0.5),
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Future<void> iniciarSesion() async {
    setState(() => estaCargando = true);
    try {
      FocusScope.of(context).unfocus();
      Usuario usuario = await _usuarioService.login(nombreUsuario, contrasenia);
      await Provider.of<UsuarioState>(context, listen: false)
          .setUsuario(usuario);
      Navigator.pushReplacementNamed(context, MenuScreen.id);
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }
}
