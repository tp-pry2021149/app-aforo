/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// ignore_for_file: public_member_api_docs

import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Usuario type in your schema. */
@immutable
class Usuario extends Model {
  static const classType = const _UsuarioModelType();
  final String id;
  final String? _nombres;
  final String? _apellidos;
  final String? _nombreUsuario;
  final String? _contrasenia;
  final String? _correoElectronico;
  final String? _urlImagen;
  final List<String>? _establecimientosFavoritos;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  String? get nombres {
    return _nombres;
  }
  
  String? get apellidos {
    return _apellidos;
  }
  
  String? get nombreUsuario {
    return _nombreUsuario;
  }
  
  String? get contrasenia {
    return _contrasenia;
  }
  
  String? get correoElectronico {
    return _correoElectronico;
  }
  
  String? get urlImagen {
    return _urlImagen;
  }
  
  List<String>? get establecimientosFavoritos {
    return _establecimientosFavoritos;
  }
  
  const Usuario._internal({required this.id, nombres, apellidos, nombreUsuario, contrasenia, correoElectronico, urlImagen, establecimientosFavoritos}): _nombres = nombres, _apellidos = apellidos, _nombreUsuario = nombreUsuario, _contrasenia = contrasenia, _correoElectronico = correoElectronico, _urlImagen = urlImagen, _establecimientosFavoritos = establecimientosFavoritos;
  
  factory Usuario({String? id, String? nombres, String? apellidos, String? nombreUsuario, String? contrasenia, String? correoElectronico, String? urlImagen, List<String>? establecimientosFavoritos}) {
    return Usuario._internal(
      id: id == null ? UUID.getUUID() : id,
      nombres: nombres,
      apellidos: apellidos,
      nombreUsuario: nombreUsuario,
      contrasenia: contrasenia,
      correoElectronico: correoElectronico,
      urlImagen: urlImagen,
      establecimientosFavoritos: establecimientosFavoritos != null ? List<String>.unmodifiable(establecimientosFavoritos) : establecimientosFavoritos);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Usuario &&
      id == other.id &&
      _nombres == other._nombres &&
      _apellidos == other._apellidos &&
      _nombreUsuario == other._nombreUsuario &&
      _contrasenia == other._contrasenia &&
      _correoElectronico == other._correoElectronico &&
      _urlImagen == other._urlImagen &&
      DeepCollectionEquality().equals(_establecimientosFavoritos, other._establecimientosFavoritos);
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Usuario {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("nombres=" + "$_nombres" + ", ");
    buffer.write("apellidos=" + "$_apellidos" + ", ");
    buffer.write("nombreUsuario=" + "$_nombreUsuario" + ", ");
    buffer.write("contrasenia=" + "$_contrasenia" + ", ");
    buffer.write("correoElectronico=" + "$_correoElectronico" + ", ");
    buffer.write("urlImagen=" + "$_urlImagen" + ", ");
    buffer.write("establecimientosFavoritos=" + (_establecimientosFavoritos != null ? _establecimientosFavoritos!.toString() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Usuario copyWith({String? id, String? nombres, String? apellidos, String? nombreUsuario, String? contrasenia, String? correoElectronico, String? urlImagen, List<String>? establecimientosFavoritos}) {
    return Usuario(
      id: id ?? this.id,
      nombres: nombres ?? this.nombres,
      apellidos: apellidos ?? this.apellidos,
      nombreUsuario: nombreUsuario ?? this.nombreUsuario,
      contrasenia: contrasenia ?? this.contrasenia,
      correoElectronico: correoElectronico ?? this.correoElectronico,
      urlImagen: urlImagen ?? this.urlImagen,
      establecimientosFavoritos: establecimientosFavoritos ?? this.establecimientosFavoritos);
  }
  
  Usuario.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _nombres = json['nombres'],
      _apellidos = json['apellidos'],
      _nombreUsuario = json['nombreUsuario'],
      _contrasenia = json['contrasenia'],
      _correoElectronico = json['correoElectronico'],
      _urlImagen = json['urlImagen'],
      _establecimientosFavoritos = json['establecimientosFavoritos']?.cast<String>();
  
  Map<String, dynamic> toJson() => {
    'id': id, 'nombres': _nombres, 'apellidos': _apellidos, 'nombreUsuario': _nombreUsuario, 'contrasenia': _contrasenia, 'correoElectronico': _correoElectronico, 'urlImagen': _urlImagen, 'establecimientosFavoritos': _establecimientosFavoritos
  };

  static final QueryField ID = QueryField(fieldName: "usuario.id");
  static final QueryField NOMBRES = QueryField(fieldName: "nombres");
  static final QueryField APELLIDOS = QueryField(fieldName: "apellidos");
  static final QueryField NOMBREUSUARIO = QueryField(fieldName: "nombreUsuario");
  static final QueryField CONTRASENIA = QueryField(fieldName: "contrasenia");
  static final QueryField CORREOELECTRONICO = QueryField(fieldName: "correoElectronico");
  static final QueryField URLIMAGEN = QueryField(fieldName: "urlImagen");
  static final QueryField ESTABLECIMIENTOSFAVORITOS = QueryField(fieldName: "establecimientosFavoritos");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Usuario";
    modelSchemaDefinition.pluralName = "Usuarios";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PUBLIC,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.NOMBRES,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.APELLIDOS,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.NOMBREUSUARIO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.CONTRASENIA,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.CORREOELECTRONICO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.URLIMAGEN,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Usuario.ESTABLECIMIENTOSFAVORITOS,
      isRequired: false,
      isArray: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.collection, ofModelName: describeEnum(ModelFieldTypeEnum.string))
    ));
  });
}

class _UsuarioModelType extends ModelType<Usuario> {
  const _UsuarioModelType();
  
  @override
  Usuario fromJson(Map<String, dynamic> jsonData) {
    return Usuario.fromJson(jsonData);
  }
}