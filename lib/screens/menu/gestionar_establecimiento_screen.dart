import 'package:app_aforo/components/eliminar_establecimiento_dialog.dart';
import 'package:app_aforo/components/establecimiento_item.dart';
import 'package:app_aforo/models/Establecimiento.dart';
import 'package:app_aforo/screens/menu/aforos_screen.dart';
import 'package:app_aforo/screens/menu/editar_establecimiento_step1_screen.dart';
import 'package:app_aforo/screens/menu/register_establecimiento_step1_screen.dart';
import 'package:app_aforo/services/establecimiento_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GestionarEstablecimientoScreen extends StatefulWidget {
  static const String id = 'gestionar_establecimiento_screen';

  @override
  _GestionarEstablecimientoScreenState createState() =>
      _GestionarEstablecimientoScreenState();
}

class _GestionarEstablecimientoScreenState
    extends State<GestionarEstablecimientoScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _establecimientoService = EstablecimientoService.instance;
  bool estaCargando = false;
  late List<Establecimiento> establecimientos = [];
  late bool hayEstablecimientos = true;

  @override
  void initState() {
    super.initState();
    obtenerEstablecimientosByUsuario();
  }

  Future<void> obtenerEstablecimientosByUsuario() async {
    setState(() => estaCargando = true);
    try {
      final usuario = Provider.of<UsuarioState>(context, listen: false).usuario;
      establecimientos = await _establecimientoService
          .getEstablecimientosByUsuarioId(usuarioId: usuario.id);
      if (establecimientos.length == 0) {
        setState(() {
          hayEstablecimientos = false;
        });
      }
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text('Gestionar establecimientos'),
          backgroundColor: kGreenPrimaryColor,
        ),
        body: Stack(
          children: [
            SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: ListView.builder(
                        itemCount: establecimientos.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            child: EstablecimientoItem(
                              establecimiento: establecimientos[index],
                              esEditable: true,
                              onTapEliminar: () async {
                                bool? respuesta =
                                    await mostrarDialogoEliminar(context);
                                if (respuesta != null) {
                                  await _establecimientoService
                                      .eliminarEstablecimiento(
                                          establecimientos[index]);
                                  setState(() {
                                    obtenerEstablecimientosByUsuario();
                                  });
                                }
                              },
                              onTapEditar: () async {
                                var route = CupertinoPageRoute(
                                  builder: (BuildContext context) =>
                                      EditarEstablecimientoStep1Screen(
                                    establecimiento: establecimientos[index],
                                  ),
                                );
                                await Navigator.of(context).push(route);
                                setState(() {
                                  obtenerEstablecimientosByUsuario();
                                });
                              },
                            ),
                            onTap: () async {
                              var route = CupertinoPageRoute(
                                builder: (BuildContext context) => AforosScreen(
                                  establecimientoId: establecimientos[index].id,
                                ),
                              );
                              await Navigator.of(context).push(route);
                              setState(() {
                                obtenerEstablecimientosByUsuario();
                              });
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            if (!hayEstablecimientos)
              Container(
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.sentiment_dissatisfied,
                        size: 50,
                        color: kGreenPrimaryColor,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'No tiene registrado ningún\nestablecimiento',
                        style: TextStyle(fontSize: 16),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: kGreenPrimaryColor,
          onPressed: () async {
            await Navigator.of(context)
                .push(CupertinoPageRoute(builder: (BuildContext context) {
              return RegisterEstablecimientoStep1Screen();
            }));
            setState(() {
              obtenerEstablecimientosByUsuario();
            });
          },
          child: Icon(
            Icons.add,
            size: 24,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Future<bool?> mostrarDialogoEliminar(BuildContext context) async {
    return await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (_) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
        ),
        contentPadding: EdgeInsets.all(0),
        content: EliminarEstablecimientoDialog(),
      ),
    );
  }
}
