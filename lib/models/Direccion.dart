// To parse this JSON data, do
//
//     final direccion = direccionFromJson(jsonString);

import 'dart:convert';

Direccion direccionFromJson(String str) => Direccion.fromMap(json.decode(str));

String direccionToJson(Direccion data) => json.encode(data.toJson());

class Direccion {
  Direccion({
    required this.results,
    required this.status,
  });

  List<Result> results;
  String status;

  Direccion.fromMap(dynamic direccionData)
      : results = List<Result>.from(direccionData["results"].map((x) => Result.fromMap(x))),
        status = direccionData["status"];

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "status": status,
      };
}

class Result {
  Result({
    required this.addressComponents,
    required this.formattedAddress,
    required this.geometry,
    required this.placeId,
    required this.types,
  });

  List<AddressComponent> addressComponents;
  String formattedAddress;
  Geometry geometry;
  String placeId;
  List<String> types;

  Result.fromMap(dynamic resultData)
      : addressComponents = List<AddressComponent>.from(
            resultData["address_components"]
                .map((x) => AddressComponent.fromMap(x))),
        formattedAddress = resultData["formatted_address"],
        geometry = Geometry.fromMap(resultData["geometry"]),
        placeId = resultData["place_id"],
        types = List<String>.from(resultData["types"].map((x) => x));

  Map<String, dynamic> toJson() => {
        "address_components":
            List<dynamic>.from(addressComponents.map((x) => x.toJson())),
        "formatted_address": formattedAddress,
        "geometry": geometry.toJson(),
        "place_id": placeId,
        "types": List<dynamic>.from(types.map((x) => x)),
      };
}

class AddressComponent {
  AddressComponent({
    required this.longName,
    required this.shortName,
    required this.types,
  });

  String longName;
  String shortName;
  List<String> types;

  AddressComponent.fromMap(dynamic addressComponentData)
      : longName = addressComponentData["long_name"],
        shortName = addressComponentData["short_name"],
        types = List<String>.from(addressComponentData["types"].map((x) => x));

  Map<String, dynamic> toJson() => {
        "long_name": longName,
        "short_name": shortName,
        "types": List<dynamic>.from(types.map((x) => x)),
      };
}

class Geometry {
  Geometry({
    required this.bounds,
    required this.location,
    required this.locationType,
    required this.viewport,
  });

  Bounds bounds;
  Location location;
  String locationType;
  Bounds viewport;

  Geometry.fromMap(dynamic geometryData)
      : bounds = Bounds.fromMap(geometryData["bounds"]),
        location = Location.fromMap(geometryData["location"]),
        locationType = geometryData["location_type"],
        viewport = Bounds.fromMap(geometryData["viewport"]);

  Map<String, dynamic> toJson() => {
        "bounds": bounds.toJson(),
        "location": location.toJson(),
        "location_type": locationType,
        "viewport": viewport.toJson(),
      };
}

class Bounds {
  Bounds({
    required this.northeast,
    required this.southwest,
  });

  Location northeast;
  Location southwest;

  Bounds.fromMap(dynamic boundsData)
      : northeast = Location.fromMap(boundsData["northeast"]),
        southwest = Location.fromMap(boundsData["southwest"]);

  Map<String, dynamic> toJson() => {
        "northeast": northeast.toJson(),
        "southwest": southwest.toJson(),
      };
}

class Location {
  Location({
    required this.lat,
    required this.lng,
  });

  double lat;
  double lng;

  Location.fromMap(dynamic locationData)
      : lat = locationData["lat"].toDouble(),
        lng = locationData["lng"].toDouble();

  Map<String, dynamic> toJson() => {
        "lat": lat,
        "lng": lng,
      };
}
