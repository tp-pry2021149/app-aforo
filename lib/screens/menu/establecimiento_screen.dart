import 'package:app_aforo/components/calificacion_dialog.dart';
import 'package:app_aforo/components/publicacion_card.dart';
import 'package:app_aforo/components/rounded_button.dart';
import 'package:app_aforo/models/Calificacion.dart';
import 'package:app_aforo/models/Establecimiento.dart';
import 'package:app_aforo/models/Usuario.dart';
import 'package:app_aforo/screens/menu/perfil_usuario_publicacion_screen.dart';
import 'package:app_aforo/services/calificacion_service.dart';
import 'package:app_aforo/services/establecimiento_service.dart';
import 'package:app_aforo/services/usuario_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'datos_estadisticos_screen.dart';

class EstablecimientoScreen extends StatefulWidget {
  static const String id = 'establecimiento_screen';
  final String? establecimientoId;
  const EstablecimientoScreen({this.establecimientoId});

  @override
  _EstablecimientoScreenState createState() => _EstablecimientoScreenState();
}

class _EstablecimientoScreenState extends State<EstablecimientoScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _establecimientoService = EstablecimientoService.instance;
  final _calificacionService = CalificacionService.instance;
  final _usuarioService = UsuarioService.instance;
  bool estaCargando = false;
  bool estaAgregadoEnFavoritos = false;
  Establecimiento establecimiento = Establecimiento();
  List<Calificacion> calificaciones = [];
  late Usuario usuario;
  late double puntajePromedio = 0;
  late String nombreUsuarioEncargado = '';
  late String fechaHoraRegistro = '';
  late String porcentaje = '';
  ScrollController controller = ScrollController();
  bool scrollDow = true;

  @override
  void initState() {
    super.initState();
    obtenerEstablecimiento();
    obtenerCalificaciones();
    initEstablecimientoFavorito();
    controller.addListener(() {
      if (controller.position.pixels == controller.position.minScrollExtent) {
        setState(() {
          scrollDow = true;
        });
      }
      if (controller.position.pixels == controller.position.maxScrollExtent) {
        setState(() {
          scrollDow = false;
        });
      }
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Future<void> initEstablecimientoFavorito() async {
    setState(() => estaCargando = true);
    try {
      usuario = await _usuarioService.getUsuarioById(
        usuarioId: Provider.of<UsuarioState>(context, listen: false).usuario.id,
      );
      print(usuario.establecimientosFavoritos!);
      usuario.establecimientosFavoritos!.forEach((establecimientoId) {
        if (establecimientoId == widget.establecimientoId) {
          estaAgregadoEnFavoritos = true;
        }
      });
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  Future<void> obtenerEstablecimiento() async {
    setState(() => estaCargando = true);
    try {
      establecimiento = await _establecimientoService.getEstablecimientoById(
          establecimientoId: widget.establecimientoId);
      final usuarioEncargado = await _usuarioService.getUsuarioById(
          usuarioId: establecimiento.usuarioId);
      setState(() {
        nombreUsuarioEncargado =
            usuarioEncargado.nombres! + ' ' + usuarioEncargado.apellidos!;
        porcentaje = ((establecimiento.ultimoAforoRegistrado! /
                    establecimiento.aforoMaximo!) *
                100)
            .toStringAsFixed(2);
        fechaHoraRegistro = DateFormat('dd/MM/yyyy hh:mm a').format(
            DateTime.fromMillisecondsSinceEpoch(
                establecimiento.fechaRegistroUltimoAforo!.toSeconds() * 1000));
      });
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  Future<void> obtenerCalificaciones() async {
    setState(() => estaCargando = true);
    try {
      calificaciones = await _calificacionService.getCalificaciones(
        establecimientoId: widget.establecimientoId,
      );
      if (calificaciones.length > 0) {
        calificaciones.sort(
          (a, b) => DateTime.fromMillisecondsSinceEpoch(
                  b.fechaHoraRegistro!.toSeconds() * 1000)
              .compareTo(DateTime.fromMillisecondsSinceEpoch(
                  a.fechaHoraRegistro!.toSeconds() * 1000)),
        );
        double sumaPuntaje = 0;
        calificaciones.forEach((calificacion) {
          sumaPuntaje += calificacion.puntaje!;
        });
        setState(() {
          puntajePromedio = sumaPuntaje / calificaciones.length;
        });
      }
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  Future<void> agregarORemoverComoFavorito() async {
    usuario.establecimientosFavoritos!.forEach((establecimientoId) {
      if (establecimientoId == establecimiento.id) {
        estaAgregadoEnFavoritos = true;
      }
    });
    if (estaAgregadoEnFavoritos) {
      var establecimientosFavoritos =
          usuario.establecimientosFavoritos!.toList();
      establecimientosFavoritos.removeWhere(
          (establecimientoId) => establecimientoId == establecimiento.id);
      usuario = usuario.copyWith(
          establecimientosFavoritos: establecimientosFavoritos);
      await Provider.of<UsuarioState>(context, listen: false)
          .setUsuario(usuario);
      await _usuarioService.create(usuario);
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text('Se removió el establecimiento de favoritos'),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } else {
      var establecimientosFavoritos =
          usuario.establecimientosFavoritos!.toList();
      establecimientosFavoritos.add(establecimiento.id);
      usuario = usuario.copyWith(
          establecimientosFavoritos: establecimientosFavoritos);
      await Provider.of<UsuarioState>(context, listen: false)
          .setUsuario(usuario);
      await _usuarioService.create(usuario);
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text('Se agregó el establecimiento a favoritos'),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    }
    setState(() {
      estaAgregadoEnFavoritos = !estaAgregadoEnFavoritos;
      usuario = usuario;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text('Detalle establecimiento'),
          backgroundColor: kGreenPrimaryColor,
          actions: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
              ),
              child: GestureDetector(
                onTap: () {
                  agregarORemoverComoFavorito();
                },
                child: Icon(
                  Icons.favorite,
                  color: (estaAgregadoEnFavoritos)
                      ? Colors.red
                      : kLightGrayPrimaryColor,
                ),
              ),
            ),
          ],
        ),
        body: Stack(
          children: [
            SafeArea(
              child: SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 20,
                          horizontal: 20,
                        ),
                        child: Container(
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: kGreenPrimaryColor,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Text(
                              '${establecimiento.nombre}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          children: [
                            Expanded(
                              child: Material(
                                elevation: 5,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(20),
                                ),
                                child: Stack(
                                  clipBehavior: Clip.none,
                                  alignment: Alignment.center,
                                  children: [
                                    Container(
                                      height: 150,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        color: Colors.white,
                                        border: Border.all(
                                            color: kGreenPrimaryColor),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(15),
                                        child: Center(
                                          child: SingleChildScrollView(
                                            controller: controller,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.stretch,
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                RichText(
                                                    text: TextSpan(children: [
                                                  TextSpan(
                                                    text: 'Aforo: ',
                                                    style: TextStyle(
                                                        fontSize: 12,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        '${establecimiento.ultimoAforoRegistrado}/${establecimiento.aforoMaximo}',
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ])),
                                                SizedBox(height: 10),
                                                RichText(
                                                    text: TextSpan(children: [
                                                  TextSpan(
                                                    text: 'Porcentaje: ',
                                                    style: TextStyle(
                                                        fontSize: 12,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  TextSpan(
                                                    text: '$porcentaje%',
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ])),
                                                SizedBox(height: 10),
                                                RichText(
                                                    text: TextSpan(children: [
                                                  TextSpan(
                                                    text: 'Fecha: ',
                                                    style: TextStyle(
                                                        fontSize: 12,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  TextSpan(
                                                    text: '$fechaHoraRegistro',
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ])),
                                                SizedBox(height: 10),
                                                RichText(
                                                    text: TextSpan(children: [
                                                  TextSpan(
                                                    text: 'Dirección: ',
                                                    style: TextStyle(
                                                        fontSize: 12,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        '${establecimiento.direccion}',
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ])),
                                                SizedBox(height: 10),
                                                RichText(
                                                    text: TextSpan(children: [
                                                  TextSpan(
                                                    text: 'RUC: ',
                                                    style: TextStyle(
                                                        fontSize: 12,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        '${establecimiento.ruc}',
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ])),
                                                SizedBox(height: 10),
                                                RichText(
                                                    text: TextSpan(children: [
                                                  TextSpan(
                                                    text: 'Encargado: ',
                                                    style: TextStyle(
                                                        fontSize: 12,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        '$nombreUsuarioEncargado',
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ])),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 10,
                                      right: 10,
                                      child: GestureDetector(
                                        onTap: () {
                                          controller.animateTo(
                                            (scrollDow)
                                                ? controller
                                                    .position.maxScrollExtent
                                                : controller
                                                    .position.minScrollExtent,
                                            duration: Duration(
                                              seconds: 1,
                                            ),
                                            curve: Curves.fastOutSlowIn,
                                          );
                                        },
                                        child: Container(
                                          width: 20,
                                          height: 20,
                                          decoration: BoxDecoration(
                                            color: kGreenPrimaryColor,
                                            shape: BoxShape.circle,
                                          ),
                                          child: Icon(
                                            (scrollDow)
                                                ? Icons.arrow_downward
                                                : Icons.arrow_upward,
                                            color: Colors.white,
                                            size: 15,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            GestureDetector(
                              onTap: () async {
                                var route = CupertinoPageRoute(
                                  builder: (BuildContext context) =>
                                      DatosEstadisticosScreen(
                                    establecimientoId: establecimiento.id,
                                  ),
                                );
                                await Navigator.of(context).push(route);
                              },
                              child: Material(
                                elevation: 5,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(20),
                                ),
                                child: Container(
                                  height: 150,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(20),
                                    ),
                                    color: Colors.white,
                                    border:
                                        Border.all(color: kGreenPrimaryColor),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Column(
                                      children: [
                                        Icon(
                                          Icons.bar_chart_outlined,
                                          size: 70,
                                          color: kGreenPrimaryColor,
                                        ),
                                        Text(
                                          'Datos\nEstadísticos',
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                          ),
                                          textAlign: TextAlign.center,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RoundedButton(
                              texto: 'Comentar y Valorar',
                              color: kGreenPrimaryColor,
                              onPressed: () async {
                                String? mensaje =
                                    await mostrarDialogoCalificacion(context);
                                if (mensaje != null) {
                                  _scaffoldMessengerKey.currentState!
                                      .showSnackBar(
                                    SnackBar(
                                      backgroundColor: kGrayPrimaryColor,
                                      content: Text(mensaje),
                                      duration: Duration(
                                        seconds: kDuracionSegundosSnackBar,
                                      ),
                                    ),
                                  );
                                  obtenerCalificaciones();
                                }
                              },
                            ),
                            Center(
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                    size: 70,
                                  ),
                                  Text(
                                    '${puntajePromedio.toStringAsFixed(1)}',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: calificaciones.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            child: PublicacionCard(
                              calificacion: calificaciones[index],
                            ),
                            onTap: () {
                              var route = CupertinoPageRoute(
                                builder: (BuildContext context) =>
                                    PerfilUsuarioPublicacionScreen(
                                  usuarioId: calificaciones[index].usuarioId!,
                                  tag: calificaciones[index].id,
                                ),
                              );
                              Navigator.of(context).push(route);
                            },
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Future<String?> mostrarDialogoCalificacion(BuildContext context) async {
    return await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (_) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
        ),
        contentPadding: EdgeInsets.all(0),
        content: CalificacionDialog(
          establecimientoId: establecimiento.id,
        ),
      ),
    );
  }
}
