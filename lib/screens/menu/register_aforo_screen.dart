import 'dart:async';
import 'package:amplify_datastore/amplify_datastore.dart';
import 'package:app_aforo/components/rounded_button.dart';
import 'package:app_aforo/components/rounded_text_field.dart';
import 'package:app_aforo/models/Aforo.dart';
import 'package:app_aforo/models/Establecimiento.dart';
import 'package:app_aforo/services/establecimiento_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:notifications/notifications.dart';

class RegisterAforoScreen extends StatefulWidget {
  static const String id = 'register_aforo_screen';
  final String? establecimientoId;
  const RegisterAforoScreen({this.establecimientoId});

  @override
  _RegisterAforoScreenState createState() => _RegisterAforoScreenState();
}

class _RegisterAforoScreenState extends State<RegisterAforoScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _establecimientoService = EstablecimientoService.instance;
  Establecimiento establecimiento = Establecimiento();
  bool estaCargando = false;
  late DateTime fechaHora;
  Notifications? _notifications;
  StreamSubscription<NotificationEvent>? _subscription;
  bool sensorActivo = false;

  @override
  void initState() {
    super.initState();
    obtenerEstablecimiento();
  }

  Future<void> initPlatformState() async {
    startListening();
  }

  void onData(NotificationEvent event) {
    if (event != null) {
      if (event.title == 'Movement detected') {
        setState(() {
          establecimiento = establecimiento.copyWith(
              ultimoAforoRegistrado:
                  establecimiento.ultimoAforoRegistrado! + 1);
        });
        registrarAforo();
      } else if (event.title == 'Door or Window is open') {
        setState(() {
          establecimiento = establecimiento.copyWith(
              ultimoAforoRegistrado:
                  establecimiento.ultimoAforoRegistrado! - 1);
        });
        registrarAforo();
      }
    }
  }

  void startListening() {
    _notifications = Notifications();
    try {
      _subscription = _notifications!.notificationStream!.listen(onData);
      setState(() => sensorActivo = true);
    } on NotificationException catch (exception) {
      print(exception);
    }
  }

  void stopListening() {
    _subscription?.cancel();
    setState(() => sensorActivo = false);
  }

  Future<void> obtenerEstablecimiento() async {
    setState(() => estaCargando = true);
    try {
      establecimiento = await _establecimientoService.getEstablecimientoById(
          establecimientoId: widget.establecimientoId);
      if (establecimiento.ultimoAforoRegistrado == null) {
        setState(() {
          establecimiento = establecimiento.copyWith(ultimoAforoRegistrado: 0);
        });
      }
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  Future<void> registrarAforo() async {
    setState(() => estaCargando = true);
    try {
      establecimiento = establecimiento.copyWith(
        fechaRegistroUltimoAforo: TemporalTimestamp(fechaHora),
      );
      await _establecimientoService.create(establecimiento);
      await _establecimientoService.createAforo(
        Aforo(
          aforoMaximoEstablecimiento: establecimiento.aforoMaximo,
          cantidad: establecimiento.ultimoAforoRegistrado,
          establecimientoId: establecimiento.id,
          fechaHoraRegistro: establecimiento.fechaRegistroUltimoAforo,
          nombreEstablecimiento: establecimiento.nombre,
          usuarioId: establecimiento.usuarioId,
        ),
      );
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text('Aforo registrado correctamente'),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text(
            'Registrar aforo',
          ),
          backgroundColor: kGreenPrimaryColor,
        ),
        body: Stack(
          children: [
            SafeArea(
              child: Center(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 10,
                    ),
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            (sensorActivo) ? stopListening() : startListening();
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              border: Border.all(
                                color: kGreenPrimaryColor,
                                width: 1.0,
                              ),
                              color: (sensorActivo)
                                  ? kGreenPrimaryColor
                                  : kLightGrayPrimaryColor,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 20,
                                vertical: 10,
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    'Detectar mediante\nsensores',
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  Icon(
                                    (sensorActivo)
                                        ? Icons.sensors
                                        : Icons.sensors_off,
                                    size: 60,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            border: Border.all(
                              color: kGreenPrimaryColor,
                              width: 1.0,
                            ),
                            color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 30,
                              vertical: 20,
                            ),
                            child: Column(
                              children: [
                                Icon(
                                  Icons.groups,
                                  size: 50,
                                  color: kGreenPrimaryColor,
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 20),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            if (establecimiento
                                                    .ultimoAforoRegistrado! >
                                                0) {
                                              establecimiento =
                                                  establecimiento.copyWith(
                                                      ultimoAforoRegistrado:
                                                          establecimiento
                                                                  .ultimoAforoRegistrado! -
                                                              1);
                                            }
                                          });
                                        },
                                        child: Icon(
                                          Icons.remove_circle,
                                          size: 50,
                                          color: kGreenPrimaryColor,
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          '${establecimiento.ultimoAforoRegistrado ?? ''}',
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 40,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            if (establecimiento
                                                    .ultimoAforoRegistrado! <
                                                establecimiento.aforoMaximo!) {
                                              establecimiento =
                                                  establecimiento.copyWith(
                                                      ultimoAforoRegistrado:
                                                          establecimiento
                                                                  .ultimoAforoRegistrado! +
                                                              1);
                                            }
                                          });
                                        },
                                        child: Icon(
                                          Icons.add_circle,
                                          size: 50,
                                          color: kGreenPrimaryColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                RoundedTextField(
                                  texto: 'Aforo',
                                  onChange: (value) {
                                    setState(() {
                                      (value == '')
                                          ? establecimiento =
                                              establecimiento.copyWith(
                                                  ultimoAforoRegistrado: 0)
                                          : (int.parse(value) >= 0 &&
                                                  establecimiento
                                                          .aforoMaximo! >=
                                                      int.parse(value))
                                              ? establecimiento =
                                                  establecimiento.copyWith(
                                                      ultimoAforoRegistrado:
                                                          int.parse(value))
                                              : _scaffoldMessengerKey
                                                  .currentState!
                                                  .showSnackBar(
                                                  SnackBar(
                                                    backgroundColor:
                                                        kGrayPrimaryColor,
                                                    content:
                                                        Text('aforo inválido'),
                                                    duration:
                                                        Duration(seconds: 1),
                                                  ),
                                                );
                                    });
                                  },
                                  onTap: () {},
                                  obscureText: false,
                                  inputType: TextInputType.number,
                                  borderColor: kGreenPrimaryColor,
                                  borderRadius: 30,
                                  esNumeroEntero: true,
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  '${establecimiento.nombre}',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'Aforo Máximo: ${establecimiento.aforoMaximo}',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  (establecimiento.ultimoAforoRegistrado !=
                                          null)
                                      ? 'Porcentaje: ${((establecimiento.ultimoAforoRegistrado! / establecimiento.aforoMaximo!) * 100).toStringAsFixed(2)}%'
                                      : '',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                StreamBuilder(
                                  stream: Stream.periodic(
                                      const Duration(seconds: 1)),
                                  builder: (context, snapshot) {
                                    fechaHora = DateTime.now();
                                    return Text(
                                      '${DateFormat('dd/MM/yyyy - hh:mm:ss a').format(fechaHora)}',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                      ),
                                      textAlign: TextAlign.center,
                                    );
                                  },
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20),
                                  child: RoundedButton(
                                    texto: 'Registrar',
                                    color: kGreenPrimaryColor,
                                    onPressed: () {
                                      registrarAforo();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
