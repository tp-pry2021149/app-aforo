/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// ignore_for_file: public_member_api_docs

import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Aforo type in your schema. */
@immutable
class Aforo extends Model {
  static const classType = const _AforoModelType();
  final String id;
  final int? _aforoMaximoEstablecimiento;
  final int? _cantidad;
  final TemporalTimestamp? _fechaHoraRegistro;
  final String? _nombreEstablecimiento;
  final String? _usuarioId;
  final String? _establecimientoId;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  int? get aforoMaximoEstablecimiento {
    return _aforoMaximoEstablecimiento;
  }
  
  int? get cantidad {
    return _cantidad;
  }
  
  TemporalTimestamp? get fechaHoraRegistro {
    return _fechaHoraRegistro;
  }
  
  String? get nombreEstablecimiento {
    return _nombreEstablecimiento;
  }
  
  String? get usuarioId {
    return _usuarioId;
  }
  
  String? get establecimientoId {
    return _establecimientoId;
  }
  
  const Aforo._internal({required this.id, aforoMaximoEstablecimiento, cantidad, fechaHoraRegistro, nombreEstablecimiento, usuarioId, establecimientoId}): _aforoMaximoEstablecimiento = aforoMaximoEstablecimiento, _cantidad = cantidad, _fechaHoraRegistro = fechaHoraRegistro, _nombreEstablecimiento = nombreEstablecimiento, _usuarioId = usuarioId, _establecimientoId = establecimientoId;
  
  factory Aforo({String? id, int? aforoMaximoEstablecimiento, int? cantidad, TemporalTimestamp? fechaHoraRegistro, String? nombreEstablecimiento, String? usuarioId, String? establecimientoId}) {
    return Aforo._internal(
      id: id == null ? UUID.getUUID() : id,
      aforoMaximoEstablecimiento: aforoMaximoEstablecimiento,
      cantidad: cantidad,
      fechaHoraRegistro: fechaHoraRegistro,
      nombreEstablecimiento: nombreEstablecimiento,
      usuarioId: usuarioId,
      establecimientoId: establecimientoId);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Aforo &&
      id == other.id &&
      _aforoMaximoEstablecimiento == other._aforoMaximoEstablecimiento &&
      _cantidad == other._cantidad &&
      _fechaHoraRegistro == other._fechaHoraRegistro &&
      _nombreEstablecimiento == other._nombreEstablecimiento &&
      _usuarioId == other._usuarioId &&
      _establecimientoId == other._establecimientoId;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Aforo {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("aforoMaximoEstablecimiento=" + (_aforoMaximoEstablecimiento != null ? _aforoMaximoEstablecimiento!.toString() : "null") + ", ");
    buffer.write("cantidad=" + (_cantidad != null ? _cantidad!.toString() : "null") + ", ");
    buffer.write("fechaHoraRegistro=" + (_fechaHoraRegistro != null ? _fechaHoraRegistro!.toString() : "null") + ", ");
    buffer.write("nombreEstablecimiento=" + "$_nombreEstablecimiento" + ", ");
    buffer.write("usuarioId=" + "$_usuarioId" + ", ");
    buffer.write("establecimientoId=" + "$_establecimientoId");
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Aforo copyWith({String? id, int? aforoMaximoEstablecimiento, int? cantidad, TemporalTimestamp? fechaHoraRegistro, String? nombreEstablecimiento, String? usuarioId, String? establecimientoId}) {
    return Aforo(
      id: id ?? this.id,
      aforoMaximoEstablecimiento: aforoMaximoEstablecimiento ?? this.aforoMaximoEstablecimiento,
      cantidad: cantidad ?? this.cantidad,
      fechaHoraRegistro: fechaHoraRegistro ?? this.fechaHoraRegistro,
      nombreEstablecimiento: nombreEstablecimiento ?? this.nombreEstablecimiento,
      usuarioId: usuarioId ?? this.usuarioId,
      establecimientoId: establecimientoId ?? this.establecimientoId);
  }
  
  Aforo.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _aforoMaximoEstablecimiento = (json['aforoMaximoEstablecimiento'] as num?)?.toInt(),
      _cantidad = (json['cantidad'] as num?)?.toInt(),
      _fechaHoraRegistro = json['fechaHoraRegistro'] != null ? TemporalTimestamp.fromSeconds(json['fechaHoraRegistro']) : null,
      _nombreEstablecimiento = json['nombreEstablecimiento'],
      _usuarioId = json['usuarioId'],
      _establecimientoId = json['establecimientoId'];
  
  Map<String, dynamic> toJson() => {
    'id': id, 'aforoMaximoEstablecimiento': _aforoMaximoEstablecimiento, 'cantidad': _cantidad, 'fechaHoraRegistro': _fechaHoraRegistro?.toSeconds(), 'nombreEstablecimiento': _nombreEstablecimiento, 'usuarioId': _usuarioId, 'establecimientoId': _establecimientoId
  };

  static final QueryField ID = QueryField(fieldName: "aforo.id");
  static final QueryField AFOROMAXIMOESTABLECIMIENTO = QueryField(fieldName: "aforoMaximoEstablecimiento");
  static final QueryField CANTIDAD = QueryField(fieldName: "cantidad");
  static final QueryField FECHAHORAREGISTRO = QueryField(fieldName: "fechaHoraRegistro");
  static final QueryField NOMBREESTABLECIMIENTO = QueryField(fieldName: "nombreEstablecimiento");
  static final QueryField USUARIOID = QueryField(fieldName: "usuarioId");
  static final QueryField ESTABLECIMIENTOID = QueryField(fieldName: "establecimientoId");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Aforo";
    modelSchemaDefinition.pluralName = "Aforos";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PUBLIC,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Aforo.AFOROMAXIMOESTABLECIMIENTO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.int)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Aforo.CANTIDAD,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.int)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Aforo.FECHAHORAREGISTRO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.timestamp)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Aforo.NOMBREESTABLECIMIENTO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Aforo.USUARIOID,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Aforo.ESTABLECIMIENTOID,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
  });
}

class _AforoModelType extends ModelType<Aforo> {
  const _AforoModelType();
  
  @override
  Aforo fromJson(Map<String, dynamic> jsonData) {
    return Aforo.fromJson(jsonData);
  }
}