import 'package:app_aforo/components/rounded_button.dart';
import 'package:app_aforo/components/rounded_text_field.dart';
import 'package:app_aforo/models/Usuario.dart';
import 'package:app_aforo/screens/menu/menu_screen.dart';
import 'package:app_aforo/services/usuario_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RegisterUserScreen extends StatefulWidget {
  static const String id = 'register_user_screen';

  @override
  _RegisterUserScreenState createState() => _RegisterUserScreenState();
}

class _RegisterUserScreenState extends State<RegisterUserScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final GlobalKey<FormState> _formKey = GlobalKey();
  final _usuarioService = UsuarioService.instance;
  Usuario usuario = Usuario();
  bool estaCargando = false;

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Registro de usuario',
          ),
          backgroundColor: kGreenPrimaryColor,
        ),
        body: Stack(
          children: [
            SafeArea(
              child: Center(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 10,
                    ),
                    child: Column(
                      children: [
                        Icon(
                          Icons.person,
                          color: kGreenPrimaryColor,
                          size: 130,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            border: Border.all(
                              color: kGreenPrimaryColor,
                              width: 1.0,
                            ),
                          ),
                          child: Form(
                            key: _formKey,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 30,
                                vertical: 20,
                              ),
                              child: Column(
                                children: [
                                  RoundedTextField(
                                    suffixIcon: Icons.account_circle_sharp,
                                    texto: 'Nombres',
                                    onChange: (value) {
                                      setState(() {
                                        usuario =
                                            usuario.copyWith(nombres: value);
                                      });
                                    },
                                    obscureText: false,
                                    inputType: TextInputType.text,
                                    borderColor: kGreenPrimaryColor,
                                    borderRadius: 30,
                                    onTap: () {},
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  RoundedTextField(
                                    suffixIcon: Icons.account_circle_sharp,
                                    texto: 'Apellidos',
                                    onChange: (value) {
                                      setState(() {
                                        usuario =
                                            usuario.copyWith(apellidos: value);
                                      });
                                    },
                                    obscureText: false,
                                    inputType: TextInputType.text,
                                    borderColor: kGreenPrimaryColor,
                                    borderRadius: 30,
                                    onTap: () {},
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  RoundedTextField(
                                    suffixIcon: Icons.account_circle_sharp,
                                    texto: 'Nombre de Usuario',
                                    onChange: (value) {
                                      setState(() {
                                        usuario = usuario.copyWith(
                                            nombreUsuario: value);
                                      });
                                    },
                                    obscureText: false,
                                    inputType: TextInputType.text,
                                    borderColor: kGreenPrimaryColor,
                                    borderRadius: 30,
                                    onTap: () {},
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  RoundedTextField(
                                    suffixIcon: Icons.email,
                                    texto: 'Correo Electrónico',
                                    onChange: (value) {
                                      setState(() {
                                        usuario = usuario.copyWith(
                                            correoElectronico: value);
                                      });
                                    },
                                    obscureText: false,
                                    inputType: TextInputType.emailAddress,
                                    borderColor: kGreenPrimaryColor,
                                    borderRadius: 30,
                                    onTap: () {},
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  RoundedTextField(
                                    suffixIcon: Icons.lock,
                                    texto: 'Contraseña',
                                    onChange: (value) {
                                      setState(() {
                                        usuario = usuario.copyWith(
                                            contrasenia: value);
                                      });
                                    },
                                    obscureText: true,
                                    inputType: TextInputType.emailAddress,
                                    borderColor: kGreenPrimaryColor,
                                    borderRadius: 30,
                                    onTap: () {},
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 20,
                                    ),
                                    child: RoundedButton(
                                      texto: 'Registrarse',
                                      color: kLightGreenPrimaryColor,
                                      onPressed: () {
                                        registrar();
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Future<void> registrar() async {
    setState(() => estaCargando = true);
    try {
      FocusScope.of(context).unfocus();
      usuario = usuario.copyWith(
        urlImagen: kUrlAvatarDefecto,
        establecimientosFavoritos: [],
      );
      await _usuarioService.create(usuario);
      await Provider.of<UsuarioState>(context, listen: false)
          .setUsuario(usuario);
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text('Se registro el usuario correctamente'),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
      Navigator.pushReplacementNamed(context, MenuScreen.id);
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }
}
