import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class Utils {
  static Future<File> showDialogImagePicker(
      BuildContext context, Function updateImage) async {
    final data = await showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          content: Container(
            height: 100,
            child: Column(
              children: [
                TextButton(
                  style: TextButton.styleFrom(
                    primary: Theme.of(context).primaryColor,
                  ),
                  onPressed: () async {
                    Navigator.pop(context);
                    await uploadImage(isGalery: true, updateImage: updateImage);
                  },
                  child: const Text("Elegir Foto"),
                ),
                TextButton(
                  style: TextButton.styleFrom(
                    primary: Theme.of(context).primaryColor,
                  ),
                  onPressed: () async {
                    Navigator.pop(context);
                    await uploadImage(
                        isGalery: false, updateImage: updateImage);
                  },
                  child: const Text("Tomar una Foto"),
                ),
              ],
            ),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        );
      },
    );
    return (data == null) ? File('') : data;
  }

  static Future<void> uploadImage({
    required bool isGalery,
    required Function updateImage,
  }) async {
    final ImagePicker _picker = ImagePicker();

    final pickedFile = await _picker.pickImage(
        source: isGalery ? ImageSource.gallery : ImageSource.camera,
        maxHeight: 1024,
        maxWidth: 1024);
    if (pickedFile != null) {
      updateImage(pickedFile.path);
    }
  }

  static String enumToString(Object o) => o.toString().split('.').last;
}
