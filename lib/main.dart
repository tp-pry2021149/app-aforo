import 'package:app_aforo/screens/menu/datos_estadisticos_screen.dart';
import 'package:app_aforo/screens/menu/establecimiento_screen.dart';
import 'package:app_aforo/screens/init/init_screen.dart';
import 'package:app_aforo/screens/menu/favorito_screen.dart';
import 'package:app_aforo/screens/menu/gestionar_establecimiento_screen.dart';
import 'package:app_aforo/screens/menu/mapa_screen.dart';
import 'package:app_aforo/screens/menu/menu_screen.dart';
import 'package:app_aforo/screens/menu/perfil_edit_screen.dart';
import 'package:app_aforo/screens/menu/perfil_screen.dart';
import 'package:app_aforo/screens/menu/register_aforo_screen.dart';
import 'package:app_aforo/screens/menu/register_establecimiento_step1_screen.dart';
import 'package:app_aforo/screens/menu/register_establecimiento_step2_screen.dart';
import 'package:app_aforo/screens/user/login_screen.dart';
import 'package:app_aforo/screens/user/register_user_screen.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => UsuarioState(),
      child: MaterialApp(
        title: 'App Aforo',
        debugShowCheckedModeBanner: false,
        initialRoute: InitScreen.id,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
        ],
        supportedLocales: [
          Locale('es'),
          Locale('en'),
        ],
        theme: ThemeData.light().copyWith(
          primaryColor: kGreenPrimaryColor,
          colorScheme: ColorScheme.light(
            primary: kGreenPrimaryColor,
          ),
        ),
        routes: {
          InitScreen.id: (context) => InitScreen(),
          LoginScreen.id: (context) => LoginScreen(),
          RegisterUserScreen.id: (context) => RegisterUserScreen(),
          MenuScreen.id: (context) => MenuScreen(),
          PerfilScreen.id: (context) => PerfilScreen(),
          EstablecimientoScreen.id: (context) => EstablecimientoScreen(),
          FavoritoScreen.id: (context) => FavoritoScreen(),
          MapaScreen.id: (context) => MapaScreen(),
          DatosEstadisticosScreen.id: (context) => DatosEstadisticosScreen(),
          GestionarEstablecimientoScreen.id: (context) =>
              GestionarEstablecimientoScreen(),
          RegisterEstablecimientoStep1Screen.id: (context) =>
              RegisterEstablecimientoStep1Screen(),
          RegisterEstablecimientoStep2Screen.id: (context) =>
              RegisterEstablecimientoStep2Screen(),
          PerfilEditScreen.id: (context) => PerfilEditScreen(),
          RegisterAforoScreen.id: (context) => RegisterAforoScreen(),
        },
      ),
    );
  }
}
