import 'dart:convert';
import 'dart:developer' as dev;
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/http_exception.dart';
import 'package:http/http.dart' as http;

class HttpService {
  static HttpService _instance = HttpService._privateConstructor();
  static HttpService get instance => _instance;
  HttpService._privateConstructor();
  Map<String, String> headers = {
    'Content-Type': 'application/json',
  };

  Future<dynamic> get(String path,
      {Map<String, String>? params, Map<String, String>? headers}) async {
    dev.log('Llamado al Http Get con path $path', name: 'HttpService.get');
    var url = Uri.https('maps.googleapis.com', path, params);
    var response = await http.get(url, headers: headers);
    dynamic data;
    if (response.statusCode == 200) {
      data = jsonDecode(utf8.decode(response.bodyBytes));
    } else if (response.statusCode == 400 || response.statusCode == 401) {
      data = jsonDecode(response.body);
      throw HttpException(
        statusCode: response.statusCode,
        error: data['error'],
        message: data['message'],
      );
    } else {
      throw HttpException(message: kMensajeErrorGenerico);
    }
    dev.log('Data $data', name: 'HttpService.get');
    return data;
  }
}
