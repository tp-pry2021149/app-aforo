import 'package:app_aforo/models/Direccion.dart';
import 'package:app_aforo/services/direction_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:flutter/material.dart';

class DirectionSearchDelegate extends SearchDelegate<Result> {
  final List<Result> historial;
  DirectionSearchDelegate(this.historial);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(onPressed: () => this.query = '', icon: Icon(Icons.clear))
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () => Navigator.pop(context),
      icon: Icon(Icons.arrow_back_ios),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.trim().length == 0) {
      return Text('No se ingresó ningun valor');
    }

    final directionService = new DirectionService();

    return FutureBuilder(
      future: directionService.getDirectionByName(query),
      builder: (_, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return _showDirections(snapshot.data.results);
        } else {
          return Center(child: CircularProgressIndicator(strokeWidth: 4));
        }
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return _showDirections(this.historial);
  }

  Widget _showDirections(List<Result> direcciones) {
    return ListView.builder(
      itemCount: direcciones.length,
      itemBuilder: (context, i) {
        final direccion = direcciones[i];

        return ListTile(
          title: Text(direccion.addressComponents[0].longName),
          subtitle: Text(direccion.formattedAddress),
          trailing: Icon(Icons.location_on_sharp, color: kGreenPrimaryColor),
          onTap: () {
            this.close(context, direccion);
          },
        );
      },
    );
  }
}
