import 'package:amplify_flutter/amplify.dart';
import 'package:app_aforo/models/Calificacion.dart';
import 'package:app_aforo/models/Usuario.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'dart:developer' as dev;

class UsuarioService {
  static UsuarioService _instance = UsuarioService._privateConstructor();
  static UsuarioService get instance => _instance;
  UsuarioService._privateConstructor();

  Future<void> create(Usuario usuario) async {
    dev.log('Creando usuario en DynamoDb $usuario',
        name: 'UsuarioService.create');
    try {
      await Amplify.DataStore.save(usuario);
      actualizarUsuarioEnPublicaciones(usuario);
    } catch (e) {
      dev.log(e.toString(), name: 'UsuarioService.create');
      throw CustomException("Ocurrió un error al crear usuario");
    }
  }

  Future<Usuario> login(String nombreUsuario, String contrasenia) async {
    dev.log(
        'Obteniendo usuario con nombreUsuario: $nombreUsuario y contrasenia: $contrasenia',
        name: 'UsuarioService.getById');
    try {
      List<Usuario> usuarios = await Amplify.DataStore.query(
        Usuario.classType,
        where: Usuario.NOMBREUSUARIO.eq(nombreUsuario).and(
              Usuario.CONTRASENIA.eq(contrasenia),
            ),
      );
      if (usuarios.isEmpty) {
        throw 'No se encontró el usuario';
      }
      return usuarios.first;
    } catch (e) {
      dev.log(e.toString(), name: 'UsuarioService.login');
      throw CustomException("Ocurrió un error al iniciar sesión");
    }
  }

  Future<Usuario> getUsuarioById({String? usuarioId}) async {
    dev.log('Obteniendo usuario con id: $usuarioId',
        name: 'UsuarioService.getUsuarioById');
    try {
      List<Usuario> usuario = await Amplify.DataStore.query(
        Usuario.classType,
        where: Usuario.ID.eq(usuarioId),
      );
      if (usuario.isEmpty) {
        throw 'No se encontró el usuario';
      }
      return usuario.first;
    } catch (e) {
      dev.log(e.toString(), name: 'UsuarioService.getUsuarioById');
      throw CustomException("Ocurrió un error al obtener usuario");
    }
  }

  // TODO hacerlo mediante functions
  Future<void> actualizarUsuarioEnPublicaciones(Usuario usuario) async {
    dev.log('Actualizando data con $usuario',
        name: 'UsuarioService.actualizarUsuarioEnPublicaciones');
    try {
      List<Calificacion> calificaciones = await Amplify.DataStore.query(
        Calificacion.classType,
        where: Calificacion.USUARIOID.eq(usuario.id),
      );
      calificaciones.forEach((calificacion) {
        Amplify.DataStore.save(calificacion.copyWith(
            nombresUsuario: usuario.nombres,
            apellidosUsuario: usuario.apellidos,
            urlImagenUsuario: usuario.urlImagen));
      });
    } catch (e) {
      dev.log(e.toString(),
          name: 'UsuarioService.actualizarUsuarioEnPublicaciones');
    }
  }
}
