import 'dart:ui';

const kLoginPath = 'images/login.png';
const kEsquinaPath = 'images/esquina.png';
const kUrlAvatarDefecto =
    'https://appaforo686885949d074267856a95ed96f837d2215654-dev.s3.us-east-2.amazonaws.com/avatar_defecto.png';
const kGreenPrimaryColor = Color(0xFF29AF7C);
const kRedPrimaryColor = Color(0xFFAF2931);
const kLightGreenPrimaryColor = Color(0xFF4ECB9B);
const kBackgroundColor = Color(0xFFDBF0E8);
const kMensajeErrorGenerico = 'Ocurrió un error intente luego';
const kGrayPrimaryColor = Color(0xFF4F4F4F);
const kLightGrayPrimaryColor = Color(0xFFAAAAAA);
const kDuracionSegundosSnackBar = 2;
const kStorage = 'usuario';
const keyAPI = 'AIzaSyBZ7CpoXDDdpM67GvdKt7TrdN0QRze_SxQ';
const kUrlBase =
    'https://appaforo686885949d074267856a95ed96f837d2215654-dev.s3.us-east-2.amazonaws.com/public/';

const schoolbell = 'Schoolbell';
enum TipoEstablecimiento { Banco, CentroComercial, Restaurante, Tienda }

