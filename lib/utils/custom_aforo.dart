class CustomAforo {
  DateTime fecha;
  double promedioPorcentaje;

  CustomAforo({
    required this.fecha,
    required this.promedioPorcentaje,
  });

  @override
  String toString() {
    return 'CustomAforo{fecha: $fecha, promedioPorcentaje: $promedioPorcentaje}';
  }
}
