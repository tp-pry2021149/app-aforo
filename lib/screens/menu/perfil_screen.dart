import 'package:app_aforo/models/Usuario.dart';
import 'package:app_aforo/screens/menu/perfil_edit_screen.dart';
import 'package:app_aforo/screens/user/login_screen.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PerfilScreen extends StatefulWidget {
  static const String id = 'perfil_screen';

  @override
  _PerfilScreenState createState() => _PerfilScreenState();
}

class _PerfilScreenState extends State<PerfilScreen> {
  late Usuario usuario;

  @override
  Widget build(BuildContext context) {
    usuario = Provider.of<UsuarioState>(context, listen: false).usuario;
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: AppBar(
        title: Text('Perfil'),
        backgroundColor: kGreenPrimaryColor,
      ),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(
                top: 100,
                right: 20,
                left: 20,
                bottom: 20,
              ),
              child: Stack(
                clipBehavior: Clip.none,
                alignment: AlignmentDirectional.center,
                children: [
                  Material(
                    elevation: 5,
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.white,
                        border: Border.all(color: kGreenPrimaryColor),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                GestureDetector(
                                  child: Icon(
                                    Icons.edit,
                                    color: kGreenPrimaryColor,
                                    size: 25,
                                  ),
                                  onTap: () async {
                                    await Navigator.of(context).push(
                                        CupertinoPageRoute(
                                            builder: (BuildContext context) {
                                      return PerfilEditScreen();
                                    }));
                                    setState(() {
                                      usuario = Provider.of<UsuarioState>(
                                              context,
                                              listen: false)
                                          .usuario;
                                    });
                                  },
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              'Nombres: ${usuario.nombres}',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              'Apellidos: ${usuario.apellidos}',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              'Nombre de usuario: ${usuario.nombreUsuario}',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              'Correo Electrónico: ${usuario.correoElectronico}',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                GestureDetector(
                                  child: Icon(
                                    Icons.exit_to_app,
                                    color: kGreenPrimaryColor,
                                    size: 25,
                                  ),
                                  onTap: () async {
                                    await Provider.of<UsuarioState>(context,
                                            listen: false)
                                        .deleteUsuario();
                                    Navigator.pushNamedAndRemoveUntil(
                                      context,
                                      LoginScreen.id,
                                      (route) => false,
                                    );
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: -80,
                    child: Material(
                      elevation: 5,
                      borderRadius: BorderRadius.all(
                        Radius.circular(60),
                      ),
                      color: kBackgroundColor,
                      child: Hero(
                        tag: 'imagenUsuario',
                        child: Container(
                          width: 120,
                          height: 120,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                '${usuario.urlImagen}',
                              ),
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(60),
                            ),
                            border: Border.all(
                              color: kGreenPrimaryColor,
                              width: 2,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
