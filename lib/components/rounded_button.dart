import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final Color color;
  final String texto;
  final VoidCallback? onPressed;

  RoundedButton({
    required this.texto,
    required this.color,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: color,
      elevation: 5,
      borderRadius: BorderRadius.circular(100),
      child: SizedBox(
        height: 40,
        child: MaterialButton(
          onPressed: onPressed,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                texto,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
