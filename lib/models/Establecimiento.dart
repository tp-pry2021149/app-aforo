/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// ignore_for_file: public_member_api_docs

import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Establecimiento type in your schema. */
@immutable
class Establecimiento extends Model {
  static const classType = const _EstablecimientoModelType();
  final String id;
  final String? _nombre;
  final String? _ruc;
  final String? _direccion;
  final int? _aforoMaximo;
  final String? _usuarioId;
  final double? _latitud;
  final double? _longitud;
  final int? _ultimoAforoRegistrado;
  final TemporalTimestamp? _fechaRegistroUltimoAforo;
  final String? _tipo;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  String? get nombre {
    return _nombre;
  }
  
  String? get ruc {
    return _ruc;
  }
  
  String? get direccion {
    return _direccion;
  }
  
  int? get aforoMaximo {
    return _aforoMaximo;
  }
  
  String? get usuarioId {
    return _usuarioId;
  }
  
  double? get latitud {
    return _latitud;
  }
  
  double? get longitud {
    return _longitud;
  }
  
  int? get ultimoAforoRegistrado {
    return _ultimoAforoRegistrado;
  }
  
  TemporalTimestamp? get fechaRegistroUltimoAforo {
    return _fechaRegistroUltimoAforo;
  }
  
  String? get tipo {
    return _tipo;
  }
  
  const Establecimiento._internal({required this.id, nombre, ruc, direccion, aforoMaximo, usuarioId, latitud, longitud, ultimoAforoRegistrado, fechaRegistroUltimoAforo, tipo}): _nombre = nombre, _ruc = ruc, _direccion = direccion, _aforoMaximo = aforoMaximo, _usuarioId = usuarioId, _latitud = latitud, _longitud = longitud, _ultimoAforoRegistrado = ultimoAforoRegistrado, _fechaRegistroUltimoAforo = fechaRegistroUltimoAforo, _tipo = tipo;
  
  factory Establecimiento({String? id, String? nombre, String? ruc, String? direccion, int? aforoMaximo, String? usuarioId, double? latitud, double? longitud, int? ultimoAforoRegistrado, TemporalTimestamp? fechaRegistroUltimoAforo, String? tipo}) {
    return Establecimiento._internal(
      id: id == null ? UUID.getUUID() : id,
      nombre: nombre,
      ruc: ruc,
      direccion: direccion,
      aforoMaximo: aforoMaximo,
      usuarioId: usuarioId,
      latitud: latitud,
      longitud: longitud,
      ultimoAforoRegistrado: ultimoAforoRegistrado,
      fechaRegistroUltimoAforo: fechaRegistroUltimoAforo,
      tipo: tipo);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Establecimiento &&
      id == other.id &&
      _nombre == other._nombre &&
      _ruc == other._ruc &&
      _direccion == other._direccion &&
      _aforoMaximo == other._aforoMaximo &&
      _usuarioId == other._usuarioId &&
      _latitud == other._latitud &&
      _longitud == other._longitud &&
      _ultimoAforoRegistrado == other._ultimoAforoRegistrado &&
      _fechaRegistroUltimoAforo == other._fechaRegistroUltimoAforo &&
      _tipo == other._tipo;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Establecimiento {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("nombre=" + "$_nombre" + ", ");
    buffer.write("ruc=" + "$_ruc" + ", ");
    buffer.write("direccion=" + "$_direccion" + ", ");
    buffer.write("aforoMaximo=" + (_aforoMaximo != null ? _aforoMaximo!.toString() : "null") + ", ");
    buffer.write("usuarioId=" + "$_usuarioId" + ", ");
    buffer.write("latitud=" + (_latitud != null ? _latitud!.toString() : "null") + ", ");
    buffer.write("longitud=" + (_longitud != null ? _longitud!.toString() : "null") + ", ");
    buffer.write("ultimoAforoRegistrado=" + (_ultimoAforoRegistrado != null ? _ultimoAforoRegistrado!.toString() : "null") + ", ");
    buffer.write("fechaRegistroUltimoAforo=" + (_fechaRegistroUltimoAforo != null ? _fechaRegistroUltimoAforo!.toString() : "null") + ", ");
    buffer.write("tipo=" + "$_tipo");
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Establecimiento copyWith({String? id, String? nombre, String? ruc, String? direccion, int? aforoMaximo, String? usuarioId, double? latitud, double? longitud, int? ultimoAforoRegistrado, TemporalTimestamp? fechaRegistroUltimoAforo, String? tipo}) {
    return Establecimiento(
      id: id ?? this.id,
      nombre: nombre ?? this.nombre,
      ruc: ruc ?? this.ruc,
      direccion: direccion ?? this.direccion,
      aforoMaximo: aforoMaximo ?? this.aforoMaximo,
      usuarioId: usuarioId ?? this.usuarioId,
      latitud: latitud ?? this.latitud,
      longitud: longitud ?? this.longitud,
      ultimoAforoRegistrado: ultimoAforoRegistrado ?? this.ultimoAforoRegistrado,
      fechaRegistroUltimoAforo: fechaRegistroUltimoAforo ?? this.fechaRegistroUltimoAforo,
      tipo: tipo ?? this.tipo);
  }
  
  Establecimiento.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _nombre = json['nombre'],
      _ruc = json['ruc'],
      _direccion = json['direccion'],
      _aforoMaximo = (json['aforoMaximo'] as num?)?.toInt(),
      _usuarioId = json['usuarioId'],
      _latitud = (json['latitud'] as num?)?.toDouble(),
      _longitud = (json['longitud'] as num?)?.toDouble(),
      _ultimoAforoRegistrado = (json['ultimoAforoRegistrado'] as num?)?.toInt(),
      _fechaRegistroUltimoAforo = json['fechaRegistroUltimoAforo'] != null ? TemporalTimestamp.fromSeconds(json['fechaRegistroUltimoAforo']) : null,
      _tipo = json['tipo'];
  
  Map<String, dynamic> toJson() => {
    'id': id, 'nombre': _nombre, 'ruc': _ruc, 'direccion': _direccion, 'aforoMaximo': _aforoMaximo, 'usuarioId': _usuarioId, 'latitud': _latitud, 'longitud': _longitud, 'ultimoAforoRegistrado': _ultimoAforoRegistrado, 'fechaRegistroUltimoAforo': _fechaRegistroUltimoAforo?.toSeconds(), 'tipo': _tipo
  };

  static final QueryField ID = QueryField(fieldName: "establecimiento.id");
  static final QueryField NOMBRE = QueryField(fieldName: "nombre");
  static final QueryField RUC = QueryField(fieldName: "ruc");
  static final QueryField DIRECCION = QueryField(fieldName: "direccion");
  static final QueryField AFOROMAXIMO = QueryField(fieldName: "aforoMaximo");
  static final QueryField USUARIOID = QueryField(fieldName: "usuarioId");
  static final QueryField LATITUD = QueryField(fieldName: "latitud");
  static final QueryField LONGITUD = QueryField(fieldName: "longitud");
  static final QueryField ULTIMOAFOROREGISTRADO = QueryField(fieldName: "ultimoAforoRegistrado");
  static final QueryField FECHAREGISTROULTIMOAFORO = QueryField(fieldName: "fechaRegistroUltimoAforo");
  static final QueryField TIPO = QueryField(fieldName: "tipo");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Establecimiento";
    modelSchemaDefinition.pluralName = "Establecimientos";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PUBLIC,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.NOMBRE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.RUC,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.DIRECCION,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.AFOROMAXIMO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.int)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.USUARIOID,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.LATITUD,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.LONGITUD,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.ULTIMOAFOROREGISTRADO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.int)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.FECHAREGISTROULTIMOAFORO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.timestamp)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Establecimiento.TIPO,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
  });
}

class _EstablecimientoModelType extends ModelType<Establecimiento> {
  const _EstablecimientoModelType();
  
  @override
  Establecimiento fromJson(Map<String, dynamic> jsonData) {
    return Establecimiento.fromJson(jsonData);
  }
}