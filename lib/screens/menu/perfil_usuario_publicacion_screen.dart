import 'package:app_aforo/models/Usuario.dart';
import 'package:app_aforo/services/usuario_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:flutter/material.dart';

class PerfilUsuarioPublicacionScreen extends StatefulWidget {
  static const String id = 'perfil_usuario_publicacion_screen';
  final String usuarioId;
  final String tag;
  const PerfilUsuarioPublicacionScreen({
    required this.usuarioId,
    required this.tag,
  });

  @override
  _PerfilUsuarioPublicacionScreenState createState() =>
      _PerfilUsuarioPublicacionScreenState();
}

class _PerfilUsuarioPublicacionScreenState
    extends State<PerfilUsuarioPublicacionScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _usuarioService = UsuarioService.instance;
  bool estaCargando = false;
  late Usuario usuario = Usuario();

  @override
  void initState() {
    super.initState();
    obtenerUsuario();
  }

  Future<void> obtenerUsuario() async {
    setState(() => estaCargando = true);
    try {
      usuario = await _usuarioService.getUsuarioById(
        usuarioId: widget.usuarioId,
      );
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text('Perfil usuario'),
          backgroundColor: kGreenPrimaryColor,
        ),
        body: Stack(
          children: [
            if (!estaCargando)
              SafeArea(
                child: Center(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 100,
                        right: 20,
                        left: 20,
                        bottom: 20,
                      ),
                      child: Stack(
                        clipBehavior: Clip.none,
                        alignment: AlignmentDirectional.center,
                        children: [
                          Material(
                            elevation: 5,
                            borderRadius: BorderRadius.all(
                              Radius.circular(20),
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                color: Colors.white,
                                border: Border.all(color: kGreenPrimaryColor),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(20),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SizedBox(
                                      height: 40,
                                    ),
                                    Text(
                                      'Nombres: ${usuario.nombres}',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Text(
                                      'Apellidos: ${usuario.apellidos}',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Text(
                                      'Nombre de usuario: ${usuario.nombreUsuario}',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Text(
                                      'Correo Electrónico: ${usuario.correoElectronico}',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: -80,
                            child: Material(
                              elevation: 5,
                              borderRadius: BorderRadius.all(
                                Radius.circular(60),
                              ),
                              color: kBackgroundColor,
                              child: Hero(
                                tag: widget.tag,
                                child: Container(
                                  width: 120,
                                  height: 120,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: NetworkImage(
                                        '${usuario.urlImagen}',
                                      ),
                                      fit: BoxFit.cover,
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(60),
                                    ),
                                    border: Border.all(
                                      color: kGreenPrimaryColor,
                                      width: 2,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
