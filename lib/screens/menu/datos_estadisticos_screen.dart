import 'package:app_aforo/components/rounded_text_field.dart';
import 'package:app_aforo/services/establecimiento_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:app_aforo/utils/custom_aforo.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class DatosEstadisticosScreen extends StatefulWidget {
  static const String id = 'datos_estadisticos_screen';
  final String? establecimientoId;
  const DatosEstadisticosScreen({this.establecimientoId});

  @override
  _DatosEstadisticosScreenState createState() =>
      _DatosEstadisticosScreenState();
}

class _DatosEstadisticosScreenState extends State<DatosEstadisticosScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _establecimientoService = EstablecimientoService.instance;
  List<CustomAforo> data = [];
  late ZoomPanBehavior _zoomPanBehavior;
  late TooltipBehavior _tooltipBehavior;
  bool estaCargando = false;
  late TextEditingController controllerFecha;
  DateTime fecha = DateTime.now();

  @override
  void initState() {
    super.initState();
    controllerFecha = TextEditingController(
      text: DateFormat('dd/MM/yyyy').format(fecha),
    );
    obtenerAforos(fecha);
    // TODO VERIFICAR QUE AL HACER ZOOM SE MUESTRE LA DATA CORRECTAMENTE
    _zoomPanBehavior = ZoomPanBehavior(
      enablePinching: true,
      zoomMode: ZoomMode.x,
      enablePanning: true,
      maximumZoomLevel: 0.5,
    );
    _tooltipBehavior = TooltipBehavior(
      color: Colors.white,
      borderColor: Colors.white,
      enable: true,
      builder: (
        dynamic data,
        dynamic point,
        dynamic series,
        int pointIndex,
        int seriesIndex,
      ) {
        return Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: kGreenPrimaryColor,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Porcentaje: ${(data.promedioPorcentaje).toStringAsFixed(2)}%',
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'Hora: ${DateFormat('hh:mm a').format(data.fecha)}',
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> obtenerAforos(DateTime fecha) async {
    setState(() => estaCargando = true);
    try {
      final fechaSinHora = DateTime(fecha.year, fecha.month, fecha.day, 0, 0);
      final aforos = await _establecimientoService.getAforos(
        establecimientoId: widget.establecimientoId,
        fecha: fechaSinHora,
      );
      setState(() {
        data = [];
        aforos.forEach(
          (aforo) {
            data.add(
              CustomAforo(
                fecha: DateTime.fromMillisecondsSinceEpoch(
                    aforo.fechaHoraRegistro!.toSeconds() * 1000),
                promedioPorcentaje: double.parse(
                  ((aforo.cantidad! / aforo.aforoMaximoEstablecimiento!) * 100)
                      .toStringAsFixed(2),
                ),
              ),
            );
          },
        );
        data.sort((a, b) => a.fecha.compareTo(b.fecha));
      });
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  Future<void> selectDate(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2200),
      helpText: 'Fecha',
    );

    if (picked != null) {
      setState(() {
        fecha = picked;
        controllerFecha.text = DateFormat('dd/MM/yyyy').format(picked);
        obtenerAforos(fecha);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text("Datos estadísticos"),
          backgroundColor: kGreenPrimaryColor,
        ),
        body: Stack(
          children: [
            SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 40,
                        vertical: 20,
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: kGreenPrimaryColor,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(
                              20,
                            ),
                          ),
                          color: Colors.white,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 50,
                            vertical: 15,
                          ),
                          child: RoundedTextField(
                            texto: "fecha",
                            inputType: TextInputType.datetime,
                            obscureText: false,
                            readOnly: true,
                            borderColor: kGreenPrimaryColor,
                            borderRadius: 30.0,
                            controller: controllerFecha,
                            onTap: () {
                              selectDate(context);
                            },
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 40,
                        vertical: 20,
                      ),
                      child: Container(
                        height: 450,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: kGreenPrimaryColor,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(20),
                          ),
                          color: Colors.white,
                        ),
                        child: SfCartesianChart(
                          zoomPanBehavior: _zoomPanBehavior,
                          plotAreaBorderWidth: 0,
                          plotAreaBackgroundColor: Colors.white,
                          legend: Legend(
                            isVisible: true,
                            alignment: ChartAlignment.center,
                            position: LegendPosition.top,
                            toggleSeriesVisibility: false,
                          ),
                          primaryXAxis: CategoryAxis(
                            majorGridLines: const MajorGridLines(width: 0),
                            labelRotation: 0,
                          ),
                          tooltipBehavior: _tooltipBehavior,
                          series: <ChartSeries>[
                            SplineSeries<CustomAforo, String>(
                              dataSource: data,
                              color: Colors.red,
                              enableTooltip: true,
                              cardinalSplineTension: 0.9,
                              isVisibleInLegend: true,
                              legendItemText: "Aforo",
                              xValueMapper: (data, _) =>
                                  DateFormat('hh:mm a').format(data.fecha),
                              yValueMapper: (data, _) =>
                                  data.promedioPorcentaje,
                              legendIconType: LegendIconType.circle,
                              markerSettings: MarkerSettings(isVisible: true),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
