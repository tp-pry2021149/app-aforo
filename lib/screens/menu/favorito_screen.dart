import 'package:app_aforo/components/establecimiento_item.dart';
import 'package:app_aforo/components/rounded_text_field.dart';
import 'package:app_aforo/models/Establecimiento.dart';
import 'package:app_aforo/screens/menu/establecimiento_screen.dart';
import 'package:app_aforo/services/establecimiento_service.dart';
import 'package:app_aforo/services/usuario_service.dart';
import 'package:app_aforo/utils/constants.dart';
import 'package:app_aforo/utils/custom-exception.dart';
import 'package:app_aforo/utils/usuario_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FavoritoScreen extends StatefulWidget {
  static const String id = 'favorito_screen';

  @override
  _FavoritoScreenState createState() => _FavoritoScreenState();
}

class _FavoritoScreenState extends State<FavoritoScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  final _establecimientoService = EstablecimientoService.instance;
  final _usuarioService = UsuarioService.instance;
  bool estaCargando = false;
  late List<Establecimiento> establecimientos;
  late List<Establecimiento> establecimientosCompletos;
  var filtroOnOff = false;
  Icon filtro = Icon(Icons.filter_alt_outlined, color: Colors.white);
  var nombreBuscar = '';
  var tipo = 'Todos';
  RangeValues _currentRangeValues = const RangeValues(0, 100);
  var min = 0.0;
  var max = 100.0;
  var colorTodos = kLightGreenPrimaryColor;
  var colorBanco = Colors.white;
  var colorCentro = Colors.white;
  var colorRestaurante = Colors.white;
  var colorTienda = Colors.white;
  late bool hayEstablecimientosFavoritos = true;

  @override
  void initState() {
    super.initState();
    obtenerEstablecimientosFavoritos();
  }

  Future<void> obtenerEstablecimientosFavoritos() async {
    setState(() => estaCargando = true);
    try {
      establecimientos = [];
      establecimientosCompletos = [];
      final usuario = await _usuarioService.getUsuarioById(
        usuarioId: Provider.of<UsuarioState>(context, listen: false).usuario.id,
      );
      usuario.establecimientosFavoritos!.forEach((establecimientoId) async {
        final establecimiento = await _establecimientoService
            .getEstablecimientoById(establecimientoId: establecimientoId);
        setState(() {
          establecimientos.add(establecimiento);
          establecimientosCompletos.add(establecimiento);
        });
      });
      if (usuario.establecimientosFavoritos!.length == 0) {
        setState(() {
          hayEstablecimientosFavoritos = false;
        });
      }
    } catch (e) {
      String mensaje =
          (e is CustomException) ? e.mensaje : kMensajeErrorGenerico;
      _scaffoldMessengerKey.currentState!.showSnackBar(SnackBar(
        backgroundColor: kGrayPrimaryColor,
        content: Text(mensaje),
        duration: Duration(seconds: kDuracionSegundosSnackBar),
      ));
    } finally {
      setState(() => estaCargando = false);
    }
  }

  Future<void> modificar() async {
    if (!filtroOnOff) {
      setState(() {
        filtro = Icon(Icons.filter_alt_rounded, color: Colors.white);
        filtroOnOff = true;
      });
    } else {
      setState(() {
        filtro = Icon(Icons.filter_alt_outlined, color: Colors.white);
        filtroOnOff = false;
      });
    }
  }

  Future<void> filtrar() async {
    if (nombreBuscar != '') {
      establecimientos = [];
      establecimientosCompletos.forEach((element) {
        if (element.nombre!.toLowerCase().contains(nombreBuscar.toLowerCase()))
          establecimientos.add(element);
      });
    } else {
      establecimientos = establecimientosCompletos;
    }
    if (establecimientos != [] && tipo != 'Todos') {
      var temp = establecimientos;
      establecimientos = [];
      temp.forEach((element) {
        if (element.tipo == tipo) establecimientos.add(element);
      });
    }
    if (establecimientos != [] && (min != 0 || max != 100)) {
      var temp = establecimientos;
      establecimientos = [];
      temp.forEach((element) {
        var porcentaje =
            (element.ultimoAforoRegistrado! * 100) / element.aforoMaximo!;
        if (porcentaje > min && porcentaje < max) establecimientos.add(element);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: _scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          title: Text('Favorito'),
          backgroundColor: kGreenPrimaryColor,
          actions: [
            IconButton(
              icon: filtro,
              onPressed: modificar,
            )
          ],
        ),
        body: Stack(
          children: [
            SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    (filtroOnOff)
                        ? Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 15,
                              vertical: 15,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: kGreenPrimaryColor,
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(
                                    20,
                                  ),
                                ),
                                color: Colors.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 15,
                                  vertical: 15,
                                ),
                                child: Column(
                                  children: [
                                    RoundedTextField(
                                      texto: 'Nombre',
                                      onChange: (value) {
                                        setState(() {
                                          nombreBuscar = value;
                                          filtrar();
                                        });
                                      },
                                      obscureText: false,
                                      inputType: TextInputType.name,
                                      borderColor: kGreenPrimaryColor,
                                      borderRadius: 30,
                                      onTap: () {},
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 10,
                                        vertical: 10,
                                      ),
                                      child: Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                colorTodos =
                                                    kLightGreenPrimaryColor;
                                                colorBanco = Colors.white;
                                                colorCentro = Colors.white;
                                                colorRestaurante = Colors.white;
                                                colorTienda = Colors.white;
                                                tipo = 'Todos';
                                                filtrar();
                                              });
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: kGreenPrimaryColor,
                                                ),
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(
                                                    20,
                                                  ),
                                                ),
                                                color: colorTodos,
                                              ),
                                              child: Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    horizontal: 9,
                                                    vertical: 5,
                                                  ),
                                                  child: Text('Todos')),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 4,
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                colorTodos = Colors.white;
                                                colorBanco =
                                                    kLightGreenPrimaryColor;
                                                colorCentro = Colors.white;
                                                colorRestaurante = Colors.white;
                                                colorTienda = Colors.white;
                                                tipo = 'Banco';
                                                filtrar();
                                              });
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: kGreenPrimaryColor,
                                                ),
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(
                                                    20,
                                                  ),
                                                ),
                                                color: colorBanco,
                                              ),
                                              child: Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    horizontal: 9,
                                                    vertical: 5,
                                                  ),
                                                  child: Text('Banco')),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 4,
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                colorTodos = Colors.white;
                                                colorBanco = Colors.white;
                                                colorCentro =
                                                    kLightGreenPrimaryColor;
                                                colorRestaurante = Colors.white;
                                                colorTienda = Colors.white;
                                                tipo = 'CentroComercial';
                                                filtrar();
                                              });
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: kGreenPrimaryColor,
                                                ),
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(
                                                    20,
                                                  ),
                                                ),
                                                color: colorCentro,
                                              ),
                                              child: Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    horizontal: 9,
                                                    vertical: 5,
                                                  ),
                                                  child:
                                                      Text('CentroComercial')),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 4,
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                colorTodos = Colors.white;
                                                colorBanco = Colors.white;
                                                colorCentro = Colors.white;
                                                colorRestaurante =
                                                    kLightGreenPrimaryColor;
                                                colorTienda = Colors.white;
                                                tipo = 'Restaurante';
                                                filtrar();
                                              });
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: kGreenPrimaryColor,
                                                ),
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(
                                                    20,
                                                  ),
                                                ),
                                                color: colorRestaurante,
                                              ),
                                              child: Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    horizontal: 9,
                                                    vertical: 5,
                                                  ),
                                                  child: Text('Restaurante')),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 4,
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                colorTodos = Colors.white;
                                                colorBanco = Colors.white;
                                                colorCentro = Colors.white;
                                                colorRestaurante = Colors.white;
                                                colorTienda =
                                                    kLightGreenPrimaryColor;
                                                tipo = 'Tienda';
                                                filtrar();
                                              });
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: kGreenPrimaryColor,
                                                ),
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(
                                                    20,
                                                  ),
                                                ),
                                                color: colorTienda,
                                              ),
                                              child: Padding(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    horizontal: 9,
                                                    vertical: 5,
                                                  ),
                                                  child: Text('Tienda')),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text('Porcentaje de Aforo'),
                                    RangeSlider(
                                        values: _currentRangeValues,
                                        min: 0,
                                        max: 100,
                                        divisions: 20,
                                        labels: RangeLabels(
                                          "${_currentRangeValues.start.round().toString()}%",
                                          "${_currentRangeValues.end.round().toString()}%",
                                        ),
                                        onChanged: (RangeValues values) {
                                          setState(() {
                                            _currentRangeValues = values;
                                            min = values.start;
                                            max = values.end;
                                            filtrar();
                                          });
                                        })
                                  ],
                                ),
                              ),
                            ),
                          )
                        : Container(
                            child: null,
                          ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: ListView.builder(
                        itemCount: establecimientos.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            child: EstablecimientoItem(
                              establecimiento: establecimientos[index],
                              esEditable: false,
                              onTapEliminar: () {},
                              onTapEditar: () {},
                            ),
                            onTap: () async {
                              if (establecimientos[index]
                                          .fechaRegistroUltimoAforo !=
                                      null &&
                                  establecimientos[index]
                                          .ultimoAforoRegistrado !=
                                      null) {
                                var route = CupertinoPageRoute(
                                  builder: (BuildContext context) =>
                                      EstablecimientoScreen(
                                    establecimientoId:
                                        establecimientos[index].id,
                                  ),
                                );
                                await Navigator.of(context).push(route);
                                setState(() {
                                  obtenerEstablecimientosFavoritos();
                                });
                              }
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            if (!hayEstablecimientosFavoritos)
              Container(
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.sentiment_dissatisfied,
                        size: 50,
                        color: kGreenPrimaryColor,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'No tiene registrado ningún\nestablecimiento en favoritos',
                        style: TextStyle(fontSize: 16),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            if (estaCargando)
              Container(
                color: kBackgroundColor,
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: kGreenPrimaryColor,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
