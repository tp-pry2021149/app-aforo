class Opcion {
  String texto;
  String valor;

  Opcion({required this.texto, required this.valor});

  @override
  String toString() {
    return 'Opcion{texto: $texto, valor: $valor';
  }
}
