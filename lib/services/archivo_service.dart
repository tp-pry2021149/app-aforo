import 'package:amplify_flutter/amplify.dart';
import 'package:amplify_storage_s3/amplify_storage_s3.dart';
import 'dart:developer' as dev;
import 'dart:io';
import 'package:app_aforo/utils/custom-exception.dart';

class ArchivoService {
  static ArchivoService _instance = ArchivoService._privateConstructor();
  static ArchivoService get instance => _instance;
  ArchivoService._privateConstructor();

  Future<String> uploadImage({File? image, String? key}) async {
    dev.log('Subiendo imagen ${image?.path}',
        name: 'ArchivoService.uploadImage');
    try {
      final UploadFileResult result = await Amplify.Storage.uploadFile(
          local: image!,
          key: key!,
          options: UploadFileOptions(
            accessLevel: StorageAccessLevel.guest,
          ));
      return result.key;
    } on StorageException catch (e) {
      dev.log(e.toString(), name: 'ArchivoService.uploadImage');
      throw CustomException("Ocurrió un error al subir la imagen");
    }
  }

  Future<String> getUrlForFile(String fileKey) async {
    dev.log('Obteniendo url de la imagen con key $fileKey',
        name: 'ArchivoService.getUrlForFile');
    try {
      final result = await Amplify.Storage.getUrl(
          key: fileKey,
          options: GetUrlOptions(
            accessLevel: StorageAccessLevel.guest,
          ));
      return result.url;
    } catch (e) {
      dev.log(e.toString(), name: 'ArchivoService.getUrlForFile');
      throw CustomException("Ocurrió un error al obtener url de imagen");
    }
  }
}
